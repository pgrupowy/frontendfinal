import React from 'react';
import { StatusBar, StyleSheet, Text, TouchableHighlight, View, Button } from 'react-native';
import { connect, Provider } from 'react-redux';
import { bindActionCreators, createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';

import reducer from './reducers'
import { ActionCreators } from './actions'

import NavBar from './components/NavBar.js';
import LoginForm from './components/LoginForm.js'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      page: '/index'
    }
  }

  login = (data) => {
    this.props.login(data)
  }

  logout = () => {
    this.props.logout()
  }

  renderWelcome() {
    let content = null
    switch (this.state.page) {
      case '/login':
        content = <LoginForm onSubmit={this.login} />
        break;
      case '/register':
        break;
      case '/index':
      default:
        content = <Button
              onPress={() => {this.setState({page: '/login'})}}
              title={'Sign In'}
              color={'#1E90FF'}
            />
    }
    return (
      <View style={{padding: 10}}>
        {content}
      </View>
    )
  }

  renderLogin() {
    return (
      <View>
        <Text>Użytkownik</Text>
        <Text>Hasło</Text>
        <Text>Wyślij ></Text>
      </View>
    )
  }

  renderRegister() {
    return (
      <View>
        <Text>Użytkownik</Text>
        <Text>Imie</Text>
        <Text>Mail</Text>
        <Text>Hasło</Text>
        <Text>Rejestruj ></Text>
      </View>
    )
  }

  renderHome() {
    return (
      <View>
        <Text>Witaj {this.props.user.username}</Text>
      </View>
    )
  }

  render() {
    // console.log('trace')
    // console.log(this.state)
    const {userLogged} = this.props.user
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />
        <NavBar/>
        <Text style={styles.text}>Projekt grupowy</Text>
        {
          userLogged ? this.renderHome() : this.renderWelcome()
        }
        <TouchableHighlight onPress={this.logout}>
          <Text style={styles.text}>{this.props.user.userLogged ? 'Wyloguj' : '...'}</Text>
        </TouchableHighlight>
        <Text>Lorem ipsum dolor sit amet</Text>
        <Text>Dodatkowy tekst</Text>
      </View>
    );
  }
}

const loggerMiddleware = createLogger({ predicate: (getState, action) => __DEV__ })

function configureStore(initialState) {
  const enhancer = compose(
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware
    )
  )
  return createStore(reducer, initialState, enhancer)
}

const store = configureStore({})

function mapStateToProps (state) {
  return {
    user: state.user
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch)
}

const ConnectedApp = connect(
  mapStateToProps,
  mapDispatchToProps
)(App)


export default () => (
  <Provider store={store}>
    <ConnectedApp />
  </Provider>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: 'dodgerblue',
    fontSize: 40,
    padding: 10
  }
});
