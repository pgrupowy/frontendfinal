import createReducer from '../utils/createReducer'
import * as types from '../actions/types'

// export const user2 = createReducer({}, {

// })

// export const user = createReducer({
//     userLogged: false
// }, {
//     [types.LOGIN](state, action) {
//         console.log('fire')
//         return {
//             ...state,
//             userLogged: !state.userLogged
//         }
//     } 
// })

const validateUser = (user, password) => {
    if (user === password && user !== undefined) {
        return true
    } else {
        return false
    }
}

export const user = (state = { userLogged: false }, action) => {
    switch (action.type) {
        case types.LOGIN:
            const { username, password } = action.data

            if (validateUser(username, password)) {
                return {
                    ...state,
                    userLogged: !state.userLogged,
                    username,
                    password
                }
            } else {
                return state
            }
            break;
        case types.LOGOUT:
            return {
                userLogged: false
            }
            break;
        default:
            return state
    }
}