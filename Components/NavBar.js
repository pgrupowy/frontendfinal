import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class NavBar extends React.Component {
    render() {
        return (
            <View style={style.navbar}>
                <Text style={style.text}>Bar text</Text>
            </View>
        )
    }   
}

export const abc = (input) => input + 1

const style = StyleSheet.create({
    navbar: {
        backgroundColor: "dodgerblue",
        height: 50,
        width: '100%',
        top: 0,
        position: 'absolute',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        color: "white",
    }
})