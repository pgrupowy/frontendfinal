import React from 'react'
import { StyleSheet, TextInput, View, Button } from 'react-native'

export default class LoginForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: ''
        }
    }

    handleChange = (field, value) => {
        this.setState({
            [field]: value
        })
    }

    checkInput = () => {
        if (this.state.username === '' || this.state.password === '') {
            return false
        } else {
            return true
        }
    }

    handleSubmit = () => {
        const { onSubmit } = this.props
        if (this.checkInput()) {
            onSubmit(this.state)
        }
    }

    render() {
        return (
           <View style={styles.form}>
            <TextInput
                style={styles.input}
                placeholder={'Username'}
                onChangeText={(text) => this.handleChange('username', text)}
                value={this.state.username}
                />
            <TextInput
                style={styles.input}
                placeholder={'Password'}
                secureTextEntry={true}
                onChangeText={(text) => this.handleChange('password', text)}
                value={this.state.password}
            />
            <Button
                onPress={this.handleSubmit}
                title={'sign in'}
                color={'#1E90FF'}
            />
           </View> 
        )
    }
}

const styles = StyleSheet.create({
    form: {
        width: 200,
        padding: 30
    },
    input: {
        paddingLeft: 5,
        paddingBottom: 10,
        marginTop: -10,
        marginBottom: 10,
        marginLeft: -5,
    }
})