import * as types from './types'

export const loginAction = (data) => ({
    type: types.LOGIN,
    data
})

export const logout = () => ({
    type: types.LOGOUT
})