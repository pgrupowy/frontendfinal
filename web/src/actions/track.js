import * as types from './types'

export const loadTrackList = (data) => ({
    type: types.LOAD_TRACK_LIST,
    data
})

export const loadTrack = (data) => ({
    type: types.LOAD_TRACK,
    data
})

export const loadRelatives = (data) => ({
    type: types.LOAD_RELATIVES,
    data
})

export const resetColors = () => ({
    type: types.RESET_COLORS
})

export const loadUserForRelative = (trackId, id, user) => ({
    type: types.LOAD_USER_FOR_RELATIVE,
    trackId,
    id,
    user
})

export const toggleTrackVisibility = (id) => ({
    type: types.TOGGLE_TRACK_VISIBILITY,
    id
})

export const hideRelative = (trackId, id) => ({
    type: types.HIDE_RELATIVE,
    trackId,
    id
})

export const showRelative = (trackId, id) => ({
    type: types.SHOW_RELATIVE,
    trackId,
    id
})

export const drawNewRelatives = () => ({
    type: types.DRAW_NEW_RELATIVES
})