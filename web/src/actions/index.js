import * as userActions from './user'
import * as trackActions from './track'

export const ActionCreators = {
    userActions,
    trackActions
}