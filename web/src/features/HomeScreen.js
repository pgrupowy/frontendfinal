import React from 'react'

import Button from '../components/Button'
import TrackMap from '../components/TrackMap'

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div style={{ width: '100%' }}>    
                <TrackMap />
            </div>
        )
    }
}