import React from 'react'

import LoginForm from '../components/LoginForm'

export default class LoginScreen extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { callback } = this.props
        return (
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column'
            }}>
                <h2>Zaloguj się do CzyMinąłem</h2>
                <LoginForm onSubmit={callback} />
            </div>
        )
    }
}