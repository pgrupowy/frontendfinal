import React from 'react'

import RegisterForm from '../components/RegisterForm'

export default class RegisterScreen extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { callback } = this.props
        return (
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column'
            }}>
                <h2>Załóż nowe konto</h2>
                <RegisterForm onSubmit={callback} />
            </div>
        )
    }
}