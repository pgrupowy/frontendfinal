import React from 'react'

import Button from '../components/Button'

export default class WelcomeScreen extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { changeRoute } = this.props
        return (
            <div>
                <h2>Witaj!</h2>
                <div style={{ width: 300 }}>
                    <Button fluid={true} text='ZALOGUJ SIĘ' callback={() => { changeRoute('/login') }} />
                    <Button fluid={true} text='ZAREJESTRUJ SIĘ' callback={() => { changeRoute('/register') }} />
                </div>
            </div>
        )
    }
}