import React, { Component } from 'react';
import { connect, Provider } from 'react-redux';
import { bindActionCreators } from 'redux';

import './App.css';
import logo from './resources/4_Grayscale_logo_on_transparent_158x72.png'

import { ActionCreators } from './actions'
import configureStore from './reducers'
import Menu from './components/Menu'
import WelcomeScreen from './features/WelcomeScreen'
import LoginScreen from './features/LoginScreen'
import RegisterScreen from './features/RegisterScreen'
import HomeScreen from './features/HomeScreen'

import * as userService from './services/user'
import { uploadTrack } from './services/track'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: '/'
    }
  }

  componentDidMount = () => {
    if (sessionStorage.auth !== undefined) {
      this.props.userActions.loginAction(JSON.parse(sessionStorage.auth))
      this.changeRoute('/home')
    }
  }

  login = (data) => {
    userService.login(
      this.props.userActions.loginAction,
      data,
      this.changeRoute
    )
  }

  logout = () => {
    this.props.userActions.logout()
    userService.logout(
      this.props.userActions.logout,
      this.changeRoute
    )
  }

  register = (data) => {
    userService.register(
      data,
      this.changeRoute
    )
  }

  changeRoute = (route) => {
    this.setState({ page: route })
  }

  renderContent() {
    switch (this.state.page) {
      case '/login':
        return <LoginScreen callback={this.login} />

      case '/home':
        return <HomeScreen />

      case '/register':
        return <RegisterScreen callback={this.register} />

      default:
        return <WelcomeScreen changeRoute={this.changeRoute} />
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} alt='CzyMinalem' />
          <Menu
            path={this.state.page}
            fullName={this.props.user.fullName}
            logout={this.logout}
            changeRoute={(path) => { this.changeRoute(path) }}
          />
        </header>
        <div style={{
          backgroundColor: '#F3F9FE',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-evenly',
          alignItems: 'center',
          minHeight: 'calc(100vh - 100px)',
          overflow: 'auto'
        }}>
          {this.state.page === '/home' ? (
            <div style={{ margin: 20 }}>
              <form onSubmit={(event) => uploadTrack(event)} acceptCharset='UTF-8' encType='multipart/form-data'>
                <input
                  type='file'
                  id='file'
                  name='file'
                />
                <button> wyślij </button>
              </form>
            </div>
          ) : null}
          {this.renderContent()}
        </div>
      </div>
    );
  }
}

const store = configureStore({})

function mapStateToProps(state) {
  return {
    user: state.user,
    track: state.track,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(ActionCreators.userActions, dispatch),
    trackActions: bindActionCreators(ActionCreators.trackActions, dispatch),
  }
}

const ConnectedApp = connect(
  mapStateToProps,
  mapDispatchToProps
)(App)


export default () => (
  <Provider store={store}>
    <ConnectedApp />
  </Provider>
)
