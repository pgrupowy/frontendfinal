export const index = [
  {
    name: 'Lorem ipsum',
    id: 2
  },
  {
    name: 'Dolor sit',
    id: 4
  },
  {
    name: 'Amet',
    id: 5
  },
  {
    name: 'Consectetur',
    id: 6
  },
  {
    name: 'Adipiscing',
    id: 7
  },
]

export const getUserByTrackId = {
  fullName: 'Elon Musk'
}

export const numberFive = {
    "startDate": [
      2017,
      10,
      20,
      14,
      49,
      23
    ],
    "endDate": [
      2017,
      10,
      20,
      15,
      25,
      36
    ],
    "name": "Mój track",
    "id": 5,
    "trackPoints": [
      {
        "latitude": 54.403605,
        "longitude": 18.577163,
        "time": [
          2017,
          10,
          20,
          14,
          49,
          23
        ],
        "elevation": 19.3
      },
      {
        "latitude": 54.403497,
        "longitude": 18.57715,
        "time": [
          2017,
          10,
          20,
          14,
          49,
          32
        ],
        "elevation": 19.4
      },
      {
        "latitude": 54.403383,
        "longitude": 18.577215,
        "time": [
          2017,
          10,
          20,
          14,
          49,
          36
        ],
        "elevation": 19.5
      },
      {
        "latitude": 54.403295,
        "longitude": 18.57728,
        "time": [
          2017,
          10,
          20,
          14,
          49,
          39
        ],
        "elevation": 19.4
      },
      {
        "latitude": 54.403208,
        "longitude": 18.577325,
        "time": [
          2017,
          10,
          20,
          14,
          49,
          42
        ],
        "elevation": 19.3
      },
      {
        "latitude": 54.403108,
        "longitude": 18.577257,
        "time": [
          2017,
          10,
          20,
          14,
          49,
          46
        ],
        "elevation": 19
      },
      {
        "latitude": 54.403007,
        "longitude": 18.577315,
        "time": [
          2017,
          10,
          20,
          14,
          49,
          51
        ],
        "elevation": 19
      },
      {
        "latitude": 54.402957,
        "longitude": 18.577167,
        "time": [
          2017,
          10,
          20,
          14,
          49,
          55
        ],
        "elevation": 18.6
      },
      {
        "latitude": 54.40291,
        "longitude": 18.57699,
        "time": [
          2017,
          10,
          20,
          14,
          49,
          59
        ],
        "elevation": 18.9
      },
      {
        "latitude": 54.402823,
        "longitude": 18.576928,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          3
        ],
        "elevation": 19.1
      },
      {
        "latitude": 54.402717,
        "longitude": 18.57693,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          6
        ],
        "elevation": 18.8
      },
      {
        "latitude": 54.402625,
        "longitude": 18.576953,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          9
        ],
        "elevation": 18.3
      },
      {
        "latitude": 54.402513,
        "longitude": 18.57701,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          13
        ],
        "elevation": 17.5
      },
      {
        "latitude": 54.40242,
        "longitude": 18.577057,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          16
        ],
        "elevation": 17.1
      },
      {
        "latitude": 54.402332,
        "longitude": 18.577093,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          19
        ],
        "elevation": 17
      },
      {
        "latitude": 54.402232,
        "longitude": 18.577078,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          22
        ],
        "elevation": 17
      },
      {
        "latitude": 54.402143,
        "longitude": 18.576952,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          26
        ],
        "elevation": 17.3
      },
      {
        "latitude": 54.40204,
        "longitude": 18.5769,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          30
        ],
        "elevation": 17.6
      },
      {
        "latitude": 54.401942,
        "longitude": 18.57684,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          34
        ],
        "elevation": 17.8
      },
      {
        "latitude": 54.40189,
        "longitude": 18.576672,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          38
        ],
        "elevation": 18
      },
      {
        "latitude": 54.401807,
        "longitude": 18.576545,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          42
        ],
        "elevation": 18
      },
      {
        "latitude": 54.401708,
        "longitude": 18.576438,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          46
        ],
        "elevation": 18
      },
      {
        "latitude": 54.40164,
        "longitude": 18.576277,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          49
        ],
        "elevation": 18
      },
      {
        "latitude": 54.401577,
        "longitude": 18.57613,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          52
        ],
        "elevation": 17.9
      },
      {
        "latitude": 54.4015,
        "longitude": 18.575983,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          55
        ],
        "elevation": 17.7
      },
      {
        "latitude": 54.40145,
        "longitude": 18.575805,
        "time": [
          2017,
          10,
          20,
          14,
          50,
          58
        ],
        "elevation": 17.5
      },
      {
        "latitude": 54.401323,
        "longitude": 18.575655,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          1
        ],
        "elevation": 17.3
      },
      {
        "latitude": 54.401225,
        "longitude": 18.575543,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          4
        ],
        "elevation": 17
      },
      {
        "latitude": 54.401223,
        "longitude": 18.575385,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          7
        ],
        "elevation": 17.3
      },
      {
        "latitude": 54.401197,
        "longitude": 18.575202,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          11
        ],
        "elevation": 17.8
      },
      {
        "latitude": 54.401175,
        "longitude": 18.575045,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          14
        ],
        "elevation": 18.2
      },
      {
        "latitude": 54.40113,
        "longitude": 18.574885,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          17
        ],
        "elevation": 18.7
      },
      {
        "latitude": 54.401097,
        "longitude": 18.574715,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          20
        ],
        "elevation": 19
      },
      {
        "latitude": 54.401112,
        "longitude": 18.57456,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          24
        ],
        "elevation": 19
      },
      {
        "latitude": 54.40117,
        "longitude": 18.574422,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          29
        ],
        "elevation": 19
      },
      {
        "latitude": 54.401122,
        "longitude": 18.574222,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          33
        ],
        "elevation": 19
      },
      {
        "latitude": 54.401067,
        "longitude": 18.574078,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          36
        ],
        "elevation": 19.6
      },
      {
        "latitude": 54.401018,
        "longitude": 18.57393,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          39
        ],
        "elevation": 20
      },
      {
        "latitude": 54.401045,
        "longitude": 18.573777,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          42
        ],
        "elevation": 20
      },
      {
        "latitude": 54.401137,
        "longitude": 18.573628,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          45
        ],
        "elevation": 20.1
      },
      {
        "latitude": 54.40123,
        "longitude": 18.573502,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          48
        ],
        "elevation": 20.6
      },
      {
        "latitude": 54.401308,
        "longitude": 18.573392,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          51
        ],
        "elevation": 20.9
      },
      {
        "latitude": 54.40139,
        "longitude": 18.573285,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          54
        ],
        "elevation": 21.3
      },
      {
        "latitude": 54.401477,
        "longitude": 18.573183,
        "time": [
          2017,
          10,
          20,
          14,
          51,
          57
        ],
        "elevation": 21.8
      },
      {
        "latitude": 54.401555,
        "longitude": 18.573075,
        "time": [
          2017,
          10,
          20,
          14,
          52
        ],
        "elevation": 22.4
      },
      {
        "latitude": 54.401642,
        "longitude": 18.572963,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          3
        ],
        "elevation": 22.5
      },
      {
        "latitude": 54.401727,
        "longitude": 18.572878,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          6
        ],
        "elevation": 22.1
      },
      {
        "latitude": 54.401812,
        "longitude": 18.572777,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          9
        ],
        "elevation": 21.5
      },
      {
        "latitude": 54.401897,
        "longitude": 18.572672,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          12
        ],
        "elevation": 21.4
      },
      {
        "latitude": 54.401955,
        "longitude": 18.572537,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          16
        ],
        "elevation": 21.6
      },
      {
        "latitude": 54.402032,
        "longitude": 18.572437,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          20
        ],
        "elevation": 21.7
      },
      {
        "latitude": 54.402123,
        "longitude": 18.57234,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          25
        ],
        "elevation": 21.1
      },
      {
        "latitude": 54.402203,
        "longitude": 18.572245,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          28
        ],
        "elevation": 20.7
      },
      {
        "latitude": 54.40229,
        "longitude": 18.572155,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          31
        ],
        "elevation": 20.4
      },
      {
        "latitude": 54.402393,
        "longitude": 18.572067,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          34
        ],
        "elevation": 20.1
      },
      {
        "latitude": 54.402487,
        "longitude": 18.571987,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          37
        ],
        "elevation": 20
      },
      {
        "latitude": 54.402563,
        "longitude": 18.571865,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          41
        ],
        "elevation": 20.3
      },
      {
        "latitude": 54.402545,
        "longitude": 18.571673,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          45
        ],
        "elevation": 20.6
      },
      {
        "latitude": 54.402523,
        "longitude": 18.571497,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          48
        ],
        "elevation": 20.8
      },
      {
        "latitude": 54.402502,
        "longitude": 18.57132,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          51
        ],
        "elevation": 21
      },
      {
        "latitude": 54.402473,
        "longitude": 18.571158,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          54
        ],
        "elevation": 21
      },
      {
        "latitude": 54.402402,
        "longitude": 18.570988,
        "time": [
          2017,
          10,
          20,
          14,
          52,
          57
        ],
        "elevation": 21
      },
      {
        "latitude": 54.402333,
        "longitude": 18.57084,
        "time": [
          2017,
          10,
          20,
          14,
          53
        ],
        "elevation": 21
      },
      {
        "latitude": 54.402298,
        "longitude": 18.570673,
        "time": [
          2017,
          10,
          20,
          14,
          53,
          4
        ],
        "elevation": 21
      },
      {
        "latitude": 54.40228,
        "longitude": 18.57051,
        "time": [
          2017,
          10,
          20,
          14,
          53,
          8
        ],
        "elevation": 21
      },
      {
        "latitude": 54.402222,
        "longitude": 18.570357,
        "time": [
          2017,
          10,
          20,
          14,
          53,
          13
        ],
        "elevation": 21
      },
      {
        "latitude": 54.402292,
        "longitude": 18.570257,
        "time": [
          2017,
          10,
          20,
          14,
          53,
          17
        ],
        "elevation": 21.3
      },
      {
        "latitude": 54.402378,
        "longitude": 18.570195,
        "time": [
          2017,
          10,
          20,
          14,
          53,
          21
        ],
        "elevation": 21.5
      },
      {
        "latitude": 54.402467,
        "longitude": 18.570128,
        "time": [
          2017,
          10,
          20,
          14,
          53,
          25
        ],
        "elevation": 21.8
      },
      {
        "latitude": 54.402532,
        "longitude": 18.570008,
        "time": [
          2017,
          10,
          20,
          14,
          53,
          30
        ],
        "elevation": 22
      },
      {
        "latitude": 54.402608,
        "longitude": 18.569902,
        "time": [
          2017,
          10,
          20,
          14,
          53,
          38
        ],
        "elevation": 22.5
      },
      {
        "latitude": 54.40271,
        "longitude": 18.569788,
        "time": [
          2017,
          10,
          20,
          14,
          53,
          43
        ],
        "elevation": 22.1
      },
      {
        "latitude": 54.402792,
        "longitude": 18.569623,
        "time": [
          2017,
          10,
          20,
          14,
          53,
          46
        ],
        "elevation": 22
      },
      {
        "latitude": 54.402857,
        "longitude": 18.569467,
        "time": [
          2017,
          10,
          20,
          14,
          53,
          49
        ],
        "elevation": 21.9
      },
      {
        "latitude": 54.402888,
        "longitude": 18.56929,
        "time": [
          2017,
          10,
          20,
          14,
          53,
          52
        ],
        "elevation": 21.9
      },
      {
        "latitude": 54.402947,
        "longitude": 18.569122,
        "time": [
          2017,
          10,
          20,
          14,
          53,
          55
        ],
        "elevation": 21.6
      },
      {
        "latitude": 54.40299,
        "longitude": 18.568918,
        "time": [
          2017,
          10,
          20,
          14,
          53,
          58
        ],
        "elevation": 21.7
      },
      {
        "latitude": 54.403048,
        "longitude": 18.568758,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          1
        ],
        "elevation": 22.6
      },
      {
        "latitude": 54.403097,
        "longitude": 18.568605,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          4
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.40313,
        "longitude": 18.568453,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          7
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.403138,
        "longitude": 18.568298,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          10
        ],
        "elevation": 23.2
      },
      {
        "latitude": 54.403115,
        "longitude": 18.568103,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          14
        ],
        "elevation": 23.9
      },
      {
        "latitude": 54.40306,
        "longitude": 18.567955,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          18
        ],
        "elevation": 24.6
      },
      {
        "latitude": 54.402985,
        "longitude": 18.567825,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          22
        ],
        "elevation": 24.8
      },
      {
        "latitude": 54.402892,
        "longitude": 18.56774,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          27
        ],
        "elevation": 24
      },
      {
        "latitude": 54.40279,
        "longitude": 18.56773,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          31
        ],
        "elevation": 23.3
      },
      {
        "latitude": 54.402678,
        "longitude": 18.567792,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          35
        ],
        "elevation": 22.9
      },
      {
        "latitude": 54.40259,
        "longitude": 18.567838,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          38
        ],
        "elevation": 22.8
      },
      {
        "latitude": 54.402493,
        "longitude": 18.5679,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          41
        ],
        "elevation": 22.8
      },
      {
        "latitude": 54.4024,
        "longitude": 18.56797,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          44
        ],
        "elevation": 22.6
      },
      {
        "latitude": 54.402302,
        "longitude": 18.568028,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          47
        ],
        "elevation": 22.7
      },
      {
        "latitude": 54.402207,
        "longitude": 18.568087,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          50
        ],
        "elevation": 23.2
      },
      {
        "latitude": 54.402117,
        "longitude": 18.568148,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          53
        ],
        "elevation": 23.5
      },
      {
        "latitude": 54.402025,
        "longitude": 18.568228,
        "time": [
          2017,
          10,
          20,
          14,
          54,
          57
        ],
        "elevation": 23.3
      },
      {
        "latitude": 54.401932,
        "longitude": 18.568327,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          1
        ],
        "elevation": 23.3
      },
      {
        "latitude": 54.401853,
        "longitude": 18.56844,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          5
        ],
        "elevation": 23.7
      },
      {
        "latitude": 54.40183,
        "longitude": 18.568602,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          9
        ],
        "elevation": 23.9
      },
      {
        "latitude": 54.401817,
        "longitude": 18.568777,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          13
        ],
        "elevation": 23.3
      },
      {
        "latitude": 54.40184,
        "longitude": 18.568938,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          17
        ],
        "elevation": 22.8
      },
      {
        "latitude": 54.401872,
        "longitude": 18.569093,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          21
        ],
        "elevation": 22.6
      },
      {
        "latitude": 54.401942,
        "longitude": 18.569205,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          25
        ],
        "elevation": 22.3
      },
      {
        "latitude": 54.402037,
        "longitude": 18.569277,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          29
        ],
        "elevation": 21.5
      },
      {
        "latitude": 54.402147,
        "longitude": 18.56929,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          33
        ],
        "elevation": 20.9
      },
      {
        "latitude": 54.402245,
        "longitude": 18.569257,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          36
        ],
        "elevation": 20.7
      },
      {
        "latitude": 54.40235,
        "longitude": 18.569175,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          40
        ],
        "elevation": 20.1
      },
      {
        "latitude": 54.402453,
        "longitude": 18.569097,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          44
        ],
        "elevation": 20.1
      },
      {
        "latitude": 54.402555,
        "longitude": 18.56902,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          48
        ],
        "elevation": 20.4
      },
      {
        "latitude": 54.402662,
        "longitude": 18.568953,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          52
        ],
        "elevation": 20.8
      },
      {
        "latitude": 54.40277,
        "longitude": 18.568875,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          56
        ],
        "elevation": 21.3
      },
      {
        "latitude": 54.402852,
        "longitude": 18.568807,
        "time": [
          2017,
          10,
          20,
          14,
          55,
          59
        ],
        "elevation": 22
      },
      {
        "latitude": 54.402947,
        "longitude": 18.568692,
        "time": [
          2017,
          10,
          20,
          14,
          56,
          3
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.403037,
        "longitude": 18.568588,
        "time": [
          2017,
          10,
          20,
          14,
          56,
          7
        ],
        "elevation": 23.4
      },
      {
        "latitude": 54.40309,
        "longitude": 18.568447,
        "time": [
          2017,
          10,
          20,
          14,
          56,
          11
        ],
        "elevation": 23.4
      },
      {
        "latitude": 54.403108,
        "longitude": 18.56828,
        "time": [
          2017,
          10,
          20,
          14,
          56,
          15
        ],
        "elevation": 23.5
      },
      {
        "latitude": 54.403092,
        "longitude": 18.568125,
        "time": [
          2017,
          10,
          20,
          14,
          56,
          19
        ],
        "elevation": 24.1
      },
      {
        "latitude": 54.403068,
        "longitude": 18.56797,
        "time": [
          2017,
          10,
          20,
          14,
          56,
          23
        ],
        "elevation": 24.7
      },
      {
        "latitude": 54.40301,
        "longitude": 18.56784,
        "time": [
          2017,
          10,
          20,
          14,
          56,
          27
        ],
        "elevation": 25.1
      },
      {
        "latitude": 54.402937,
        "longitude": 18.567737,
        "time": [
          2017,
          10,
          20,
          14,
          56,
          31
        ],
        "elevation": 24.7
      },
      {
        "latitude": 54.402832,
        "longitude": 18.567693,
        "time": [
          2017,
          10,
          20,
          14,
          56,
          35
        ],
        "elevation": 23.9
      },
      {
        "latitude": 54.40272,
        "longitude": 18.567703,
        "time": [
          2017,
          10,
          20,
          14,
          56,
          39
        ],
        "elevation": 23.4
      },
      {
        "latitude": 54.40262,
        "longitude": 18.567745,
        "time": [
          2017,
          10,
          20,
          14,
          56,
          42
        ],
        "elevation": 23.4
      },
      {
        "latitude": 54.402513,
        "longitude": 18.567842,
        "time": [
          2017,
          10,
          20,
          14,
          56,
          46
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.402415,
        "longitude": 18.567925,
        "time": [
          2017,
          10,
          20,
          14,
          56,
          50
        ],
        "elevation": 23
      },
      {
        "latitude": 54.402327,
        "longitude": 18.56799,
        "time": [
          2017,
          10,
          20,
          14,
          56,
          54
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.402242,
        "longitude": 18.56805,
        "time": [
          2017,
          10,
          20,
          14,
          56,
          57
        ],
        "elevation": 23.5
      },
      {
        "latitude": 54.402148,
        "longitude": 18.568115,
        "time": [
          2017,
          10,
          20,
          14,
          57
        ],
        "elevation": 23.7
      },
      {
        "latitude": 54.402067,
        "longitude": 18.568185,
        "time": [
          2017,
          10,
          20,
          14,
          57,
          3
        ],
        "elevation": 23.7
      },
      {
        "latitude": 54.401967,
        "longitude": 18.56829,
        "time": [
          2017,
          10,
          20,
          14,
          57,
          7
        ],
        "elevation": 23.6
      },
      {
        "latitude": 54.401885,
        "longitude": 18.568397,
        "time": [
          2017,
          10,
          20,
          14,
          57,
          11
        ],
        "elevation": 23.8
      },
      {
        "latitude": 54.401832,
        "longitude": 18.568538,
        "time": [
          2017,
          10,
          20,
          14,
          57,
          15
        ],
        "elevation": 24.2
      },
      {
        "latitude": 54.401812,
        "longitude": 18.568698,
        "time": [
          2017,
          10,
          20,
          14,
          57,
          19
        ],
        "elevation": 23.8
      },
      {
        "latitude": 54.401828,
        "longitude": 18.568875,
        "time": [
          2017,
          10,
          20,
          14,
          57,
          23
        ],
        "elevation": 23
      },
      {
        "latitude": 54.40185,
        "longitude": 18.56905,
        "time": [
          2017,
          10,
          20,
          14,
          57,
          27
        ],
        "elevation": 23
      },
      {
        "latitude": 54.401895,
        "longitude": 18.569188,
        "time": [
          2017,
          10,
          20,
          14,
          57,
          32
        ],
        "elevation": 22.8
      },
      {
        "latitude": 54.401985,
        "longitude": 18.569252,
        "time": [
          2017,
          10,
          20,
          14,
          57,
          37
        ],
        "elevation": 22.2
      },
      {
        "latitude": 54.402092,
        "longitude": 18.56925,
        "time": [
          2017,
          10,
          20,
          14,
          57,
          41
        ],
        "elevation": 21.5
      },
      {
        "latitude": 54.402217,
        "longitude": 18.56923,
        "time": [
          2017,
          10,
          20,
          14,
          57,
          45
        ],
        "elevation": 20.9
      },
      {
        "latitude": 54.402328,
        "longitude": 18.56918,
        "time": [
          2017,
          10,
          20,
          14,
          57,
          49
        ],
        "elevation": 20.4
      },
      {
        "latitude": 54.402425,
        "longitude": 18.569098,
        "time": [
          2017,
          10,
          20,
          14,
          57,
          53
        ],
        "elevation": 20.3
      },
      {
        "latitude": 54.40251,
        "longitude": 18.569033,
        "time": [
          2017,
          10,
          20,
          14,
          57,
          56
        ],
        "elevation": 20.5
      },
      {
        "latitude": 54.402593,
        "longitude": 18.568973,
        "time": [
          2017,
          10,
          20,
          14,
          57,
          59
        ],
        "elevation": 20.8
      },
      {
        "latitude": 54.4027,
        "longitude": 18.568898,
        "time": [
          2017,
          10,
          20,
          14,
          58,
          3
        ],
        "elevation": 21.3
      },
      {
        "latitude": 54.402793,
        "longitude": 18.568837,
        "time": [
          2017,
          10,
          20,
          14,
          58,
          6
        ],
        "elevation": 21.8
      },
      {
        "latitude": 54.402885,
        "longitude": 18.568755,
        "time": [
          2017,
          10,
          20,
          14,
          58,
          10
        ],
        "elevation": 22.6
      },
      {
        "latitude": 54.402973,
        "longitude": 18.568672,
        "time": [
          2017,
          10,
          20,
          14,
          58,
          14
        ],
        "elevation": 23.4
      },
      {
        "latitude": 54.403063,
        "longitude": 18.568567,
        "time": [
          2017,
          10,
          20,
          14,
          58,
          18
        ],
        "elevation": 23.7
      },
      {
        "latitude": 54.403115,
        "longitude": 18.568425,
        "time": [
          2017,
          10,
          20,
          14,
          58,
          22
        ],
        "elevation": 23.6
      },
      {
        "latitude": 54.403112,
        "longitude": 18.568258,
        "time": [
          2017,
          10,
          20,
          14,
          58,
          26
        ],
        "elevation": 23.9
      },
      {
        "latitude": 54.403073,
        "longitude": 18.568102,
        "time": [
          2017,
          10,
          20,
          14,
          58,
          30
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.40305,
        "longitude": 18.567915,
        "time": [
          2017,
          10,
          20,
          14,
          58,
          35
        ],
        "elevation": 25.3
      },
      {
        "latitude": 54.40299,
        "longitude": 18.567793,
        "time": [
          2017,
          10,
          20,
          14,
          58,
          39
        ],
        "elevation": 25.4
      },
      {
        "latitude": 54.402897,
        "longitude": 18.567707,
        "time": [
          2017,
          10,
          20,
          14,
          58,
          43
        ],
        "elevation": 24.8
      },
      {
        "latitude": 54.402793,
        "longitude": 18.567682,
        "time": [
          2017,
          10,
          20,
          14,
          58,
          47
        ],
        "elevation": 23.9
      },
      {
        "latitude": 54.4027,
        "longitude": 18.567707,
        "time": [
          2017,
          10,
          20,
          14,
          58,
          50
        ],
        "elevation": 23.7
      },
      {
        "latitude": 54.402602,
        "longitude": 18.567738,
        "time": [
          2017,
          10,
          20,
          14,
          58,
          53
        ],
        "elevation": 23.7
      },
      {
        "latitude": 54.402502,
        "longitude": 18.567827,
        "time": [
          2017,
          10,
          20,
          14,
          58,
          57
        ],
        "elevation": 23.5
      },
      {
        "latitude": 54.402417,
        "longitude": 18.567898,
        "time": [
          2017,
          10,
          20,
          14,
          59
        ],
        "elevation": 23.4
      },
      {
        "latitude": 54.402315,
        "longitude": 18.56798,
        "time": [
          2017,
          10,
          20,
          14,
          59,
          4
        ],
        "elevation": 23.4
      },
      {
        "latitude": 54.402205,
        "longitude": 18.568058,
        "time": [
          2017,
          10,
          20,
          14,
          59,
          8
        ],
        "elevation": 23.8
      },
      {
        "latitude": 54.402095,
        "longitude": 18.568133,
        "time": [
          2017,
          10,
          20,
          14,
          59,
          12
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.401995,
        "longitude": 18.568215,
        "time": [
          2017,
          10,
          20,
          14,
          59,
          16
        ],
        "elevation": 24.3
      },
      {
        "latitude": 54.401918,
        "longitude": 18.56832,
        "time": [
          2017,
          10,
          20,
          14,
          59,
          20
        ],
        "elevation": 24
      },
      {
        "latitude": 54.401855,
        "longitude": 18.568448,
        "time": [
          2017,
          10,
          20,
          14,
          59,
          24
        ],
        "elevation": 24.3
      },
      {
        "latitude": 54.401833,
        "longitude": 18.568613,
        "time": [
          2017,
          10,
          20,
          14,
          59,
          28
        ],
        "elevation": 24.3
      },
      {
        "latitude": 54.401832,
        "longitude": 18.568798,
        "time": [
          2017,
          10,
          20,
          14,
          59,
          32
        ],
        "elevation": 23.6
      },
      {
        "latitude": 54.40186,
        "longitude": 18.568978,
        "time": [
          2017,
          10,
          20,
          14,
          59,
          36
        ],
        "elevation": 23.2
      },
      {
        "latitude": 54.401895,
        "longitude": 18.56913,
        "time": [
          2017,
          10,
          20,
          14,
          59,
          40
        ],
        "elevation": 23.2
      },
      {
        "latitude": 54.401958,
        "longitude": 18.56924,
        "time": [
          2017,
          10,
          20,
          14,
          59,
          44
        ],
        "elevation": 22.6
      },
      {
        "latitude": 54.402058,
        "longitude": 18.569335,
        "time": [
          2017,
          10,
          20,
          14,
          59,
          48
        ],
        "elevation": 21.9
      },
      {
        "latitude": 54.402167,
        "longitude": 18.569357,
        "time": [
          2017,
          10,
          20,
          14,
          59,
          52
        ],
        "elevation": 21.7
      },
      {
        "latitude": 54.402257,
        "longitude": 18.569302,
        "time": [
          2017,
          10,
          20,
          14,
          59,
          56
        ],
        "elevation": 21.4
      },
      {
        "latitude": 54.402355,
        "longitude": 18.569212,
        "time": [
          2017,
          10,
          20,
          15,
          0
        ],
        "elevation": 20.8
      },
      {
        "latitude": 54.40245,
        "longitude": 18.569117,
        "time": [
          2017,
          10,
          20,
          15,
          0,
          4
        ],
        "elevation": 20.6
      },
      {
        "latitude": 54.402553,
        "longitude": 18.56902,
        "time": [
          2017,
          10,
          20,
          15,
          0,
          8
        ],
        "elevation": 20.9
      },
      {
        "latitude": 54.402637,
        "longitude": 18.568958,
        "time": [
          2017,
          10,
          20,
          15,
          0,
          11
        ],
        "elevation": 21.2
      },
      {
        "latitude": 54.402718,
        "longitude": 18.568893,
        "time": [
          2017,
          10,
          20,
          15,
          0,
          14
        ],
        "elevation": 21.6
      },
      {
        "latitude": 54.40282,
        "longitude": 18.568812,
        "time": [
          2017,
          10,
          20,
          15,
          0,
          18
        ],
        "elevation": 22.3
      },
      {
        "latitude": 54.402918,
        "longitude": 18.568733,
        "time": [
          2017,
          10,
          20,
          15,
          0,
          22
        ],
        "elevation": 23.2
      },
      {
        "latitude": 54.403012,
        "longitude": 18.568638,
        "time": [
          2017,
          10,
          20,
          15,
          0,
          26
        ],
        "elevation": 23.8
      },
      {
        "latitude": 54.40309,
        "longitude": 18.5685,
        "time": [
          2017,
          10,
          20,
          15,
          0,
          30
        ],
        "elevation": 23.9
      },
      {
        "latitude": 54.403118,
        "longitude": 18.568333,
        "time": [
          2017,
          10,
          20,
          15,
          0,
          34
        ],
        "elevation": 23.9
      },
      {
        "latitude": 54.403108,
        "longitude": 18.568152,
        "time": [
          2017,
          10,
          20,
          15,
          0,
          38
        ],
        "elevation": 24.5
      },
      {
        "latitude": 54.403082,
        "longitude": 18.567977,
        "time": [
          2017,
          10,
          20,
          15,
          0,
          42
        ],
        "elevation": 25.3
      },
      {
        "latitude": 54.403027,
        "longitude": 18.567838,
        "time": [
          2017,
          10,
          20,
          15,
          0,
          46
        ],
        "elevation": 25.8
      },
      {
        "latitude": 54.402945,
        "longitude": 18.567718,
        "time": [
          2017,
          10,
          20,
          15,
          0,
          50
        ],
        "elevation": 25.3
      },
      {
        "latitude": 54.402833,
        "longitude": 18.567673,
        "time": [
          2017,
          10,
          20,
          15,
          0,
          54
        ],
        "elevation": 24.5
      },
      {
        "latitude": 54.402723,
        "longitude": 18.567693,
        "time": [
          2017,
          10,
          20,
          15,
          0,
          58
        ],
        "elevation": 24
      },
      {
        "latitude": 54.402638,
        "longitude": 18.567745,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          1
        ],
        "elevation": 23.9
      },
      {
        "latitude": 54.40255,
        "longitude": 18.567802,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          4
        ],
        "elevation": 23.7
      },
      {
        "latitude": 54.402447,
        "longitude": 18.5679,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          8
        ],
        "elevation": 23.6
      },
      {
        "latitude": 54.402348,
        "longitude": 18.567992,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          12
        ],
        "elevation": 23.5
      },
      {
        "latitude": 54.402245,
        "longitude": 18.568063,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          16
        ],
        "elevation": 23.9
      },
      {
        "latitude": 54.402133,
        "longitude": 18.56813,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          20
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.402027,
        "longitude": 18.568212,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          24
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.401923,
        "longitude": 18.5683,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          28
        ],
        "elevation": 24.3
      },
      {
        "latitude": 54.401833,
        "longitude": 18.568428,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          32
        ],
        "elevation": 24.8
      },
      {
        "latitude": 54.401778,
        "longitude": 18.568577,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          36
        ],
        "elevation": 24.9
      },
      {
        "latitude": 54.401782,
        "longitude": 18.568765,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          40
        ],
        "elevation": 24.2
      },
      {
        "latitude": 54.401802,
        "longitude": 18.568937,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          44
        ],
        "elevation": 23.6
      },
      {
        "latitude": 54.40184,
        "longitude": 18.569095,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          48
        ],
        "elevation": 23.7
      },
      {
        "latitude": 54.401918,
        "longitude": 18.56921,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          51
        ],
        "elevation": 23.2
      },
      {
        "latitude": 54.402017,
        "longitude": 18.569292,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          55
        ],
        "elevation": 22.5
      },
      {
        "latitude": 54.402123,
        "longitude": 18.569318,
        "time": [
          2017,
          10,
          20,
          15,
          1,
          59
        ],
        "elevation": 21.9
      },
      {
        "latitude": 54.402213,
        "longitude": 18.5693,
        "time": [
          2017,
          10,
          20,
          15,
          2,
          2
        ],
        "elevation": 21.7
      },
      {
        "latitude": 54.402325,
        "longitude": 18.569227,
        "time": [
          2017,
          10,
          20,
          15,
          2,
          6
        ],
        "elevation": 21.1
      },
      {
        "latitude": 54.402435,
        "longitude": 18.569142,
        "time": [
          2017,
          10,
          20,
          15,
          2,
          10
        ],
        "elevation": 20.9
      },
      {
        "latitude": 54.402542,
        "longitude": 18.56906,
        "time": [
          2017,
          10,
          20,
          15,
          2,
          14
        ],
        "elevation": 21.1
      },
      {
        "latitude": 54.402638,
        "longitude": 18.568965,
        "time": [
          2017,
          10,
          20,
          15,
          2,
          18
        ],
        "elevation": 21.5
      },
      {
        "latitude": 54.402735,
        "longitude": 18.568877,
        "time": [
          2017,
          10,
          20,
          15,
          2,
          22
        ],
        "elevation": 22
      },
      {
        "latitude": 54.402842,
        "longitude": 18.568785,
        "time": [
          2017,
          10,
          20,
          15,
          2,
          26
        ],
        "elevation": 22.7
      },
      {
        "latitude": 54.402927,
        "longitude": 18.568712,
        "time": [
          2017,
          10,
          20,
          15,
          2,
          29
        ],
        "elevation": 23.6
      },
      {
        "latitude": 54.40301,
        "longitude": 18.56864,
        "time": [
          2017,
          10,
          20,
          15,
          2,
          32
        ],
        "elevation": 24.1
      },
      {
        "latitude": 54.403093,
        "longitude": 18.568527,
        "time": [
          2017,
          10,
          20,
          15,
          2,
          36
        ],
        "elevation": 24.2
      },
      {
        "latitude": 54.403128,
        "longitude": 18.56837,
        "time": [
          2017,
          10,
          20,
          15,
          2,
          40
        ],
        "elevation": 24.1
      },
      {
        "latitude": 54.40313,
        "longitude": 18.568195,
        "time": [
          2017,
          10,
          20,
          15,
          2,
          44
        ],
        "elevation": 24.6
      },
      {
        "latitude": 54.403102,
        "longitude": 18.56803,
        "time": [
          2017,
          10,
          20,
          15,
          2,
          48
        ],
        "elevation": 25.4
      },
      {
        "latitude": 54.40305,
        "longitude": 18.567883,
        "time": [
          2017,
          10,
          20,
          15,
          2,
          52
        ],
        "elevation": 25.9
      },
      {
        "latitude": 54.402965,
        "longitude": 18.567752,
        "time": [
          2017,
          10,
          20,
          15,
          2,
          56
        ],
        "elevation": 25.7
      },
      {
        "latitude": 54.402863,
        "longitude": 18.56767,
        "time": [
          2017,
          10,
          20,
          15,
          3
        ],
        "elevation": 24.9
      },
      {
        "latitude": 54.40277,
        "longitude": 18.56765,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          3
        ],
        "elevation": 24.5
      },
      {
        "latitude": 54.402667,
        "longitude": 18.567695,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          7
        ],
        "elevation": 24.3
      },
      {
        "latitude": 54.40258,
        "longitude": 18.567752,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          10
        ],
        "elevation": 24.3
      },
      {
        "latitude": 54.402487,
        "longitude": 18.56785,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          14
        ],
        "elevation": 24
      },
      {
        "latitude": 54.402388,
        "longitude": 18.567943,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          18
        ],
        "elevation": 23.9
      },
      {
        "latitude": 54.402308,
        "longitude": 18.568017,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          21
        ],
        "elevation": 23.8
      },
      {
        "latitude": 54.402223,
        "longitude": 18.568078,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          24
        ],
        "elevation": 24.2
      },
      {
        "latitude": 54.402135,
        "longitude": 18.568122,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          27
        ],
        "elevation": 24.7
      },
      {
        "latitude": 54.402045,
        "longitude": 18.568163,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          30
        ],
        "elevation": 25
      },
      {
        "latitude": 54.401935,
        "longitude": 18.568238,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          34
        ],
        "elevation": 24.8
      },
      {
        "latitude": 54.40186,
        "longitude": 18.568328,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          37
        ],
        "elevation": 25
      },
      {
        "latitude": 54.401815,
        "longitude": 18.568482,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          41
        ],
        "elevation": 25.2
      },
      {
        "latitude": 54.40178,
        "longitude": 18.56866,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          45
        ],
        "elevation": 24.9
      },
      {
        "latitude": 54.401777,
        "longitude": 18.568827,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          49
        ],
        "elevation": 24.1
      },
      {
        "latitude": 54.401797,
        "longitude": 18.568983,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          53
        ],
        "elevation": 23.9
      },
      {
        "latitude": 54.401858,
        "longitude": 18.569135,
        "time": [
          2017,
          10,
          20,
          15,
          3,
          57
        ],
        "elevation": 23.8
      },
      {
        "latitude": 54.40194,
        "longitude": 18.569237,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          1
        ],
        "elevation": 23.3
      },
      {
        "latitude": 54.40204,
        "longitude": 18.56929,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          5
        ],
        "elevation": 22.6
      },
      {
        "latitude": 54.402153,
        "longitude": 18.569268,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          9
        ],
        "elevation": 22
      },
      {
        "latitude": 54.402247,
        "longitude": 18.569228,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          12
        ],
        "elevation": 21.5
      },
      {
        "latitude": 54.40235,
        "longitude": 18.569147,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          16
        ],
        "elevation": 21.1
      },
      {
        "latitude": 54.402442,
        "longitude": 18.56906,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          20
        ],
        "elevation": 21.1
      },
      {
        "latitude": 54.40253,
        "longitude": 18.568982,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          24
        ],
        "elevation": 21.4
      },
      {
        "latitude": 54.402628,
        "longitude": 18.56892,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          28
        ],
        "elevation": 21.8
      },
      {
        "latitude": 54.402715,
        "longitude": 18.568815,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          32
        ],
        "elevation": 22.4
      },
      {
        "latitude": 54.402803,
        "longitude": 18.568753,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          35
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.402903,
        "longitude": 18.568703,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          38
        ],
        "elevation": 23.8
      },
      {
        "latitude": 54.403002,
        "longitude": 18.568618,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          42
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.403085,
        "longitude": 18.568482,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          46
        ],
        "elevation": 24.5
      },
      {
        "latitude": 54.403123,
        "longitude": 18.568335,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          50
        ],
        "elevation": 24.5
      },
      {
        "latitude": 54.403113,
        "longitude": 18.568142,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          55
        ],
        "elevation": 25
      },
      {
        "latitude": 54.403082,
        "longitude": 18.567987,
        "time": [
          2017,
          10,
          20,
          15,
          4,
          59
        ],
        "elevation": 25.8
      },
      {
        "latitude": 54.403012,
        "longitude": 18.567838,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          3
        ],
        "elevation": 26.3
      },
      {
        "latitude": 54.402927,
        "longitude": 18.567738,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          7
        ],
        "elevation": 25.8
      },
      {
        "latitude": 54.402822,
        "longitude": 18.567705,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          11
        ],
        "elevation": 25
      },
      {
        "latitude": 54.402708,
        "longitude": 18.567725,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          15
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.40261,
        "longitude": 18.56776,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          18
        ],
        "elevation": 24.3
      },
      {
        "latitude": 54.40252,
        "longitude": 18.567803,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          21
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.402413,
        "longitude": 18.56788,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          25
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.402305,
        "longitude": 18.567957,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          29
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.402203,
        "longitude": 18.568032,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          33
        ],
        "elevation": 24.7
      },
      {
        "latitude": 54.402115,
        "longitude": 18.568118,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          37
        ],
        "elevation": 25.1
      },
      {
        "latitude": 54.402013,
        "longitude": 18.568197,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          41
        ],
        "elevation": 25
      },
      {
        "latitude": 54.40191,
        "longitude": 18.568273,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          45
        ],
        "elevation": 25
      },
      {
        "latitude": 54.401827,
        "longitude": 18.56838,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          49
        ],
        "elevation": 25.4
      },
      {
        "latitude": 54.40178,
        "longitude": 18.568538,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          53
        ],
        "elevation": 25.5
      },
      {
        "latitude": 54.40177,
        "longitude": 18.568715,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          57
        ],
        "elevation": 25
      },
      {
        "latitude": 54.401787,
        "longitude": 18.568902,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          1
        ],
        "elevation": 24.3
      },
      {
        "latitude": 54.40184,
        "longitude": 18.56907,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          5
        ],
        "elevation": 24.2
      },
      {
        "latitude": 54.40192,
        "longitude": 18.56921,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          9
        ],
        "elevation": 23.8
      },
      {
        "latitude": 54.402003,
        "longitude": 18.569275,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          13
        ],
        "elevation": 23
      },
      {
        "latitude": 54.4021,
        "longitude": 18.569288,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          17
        ],
        "elevation": 23
      },
      {
        "latitude": 54.402198,
        "longitude": 18.569273,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          20
        ],
        "elevation": 22.9
      },
      {
        "latitude": 54.4023,
        "longitude": 18.569248,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          23
        ],
        "elevation": 22.5
      },
      {
        "latitude": 54.40241,
        "longitude": 18.569182,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          27
        ],
        "elevation": 21.9
      },
      {
        "latitude": 54.402493,
        "longitude": 18.569117,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          30
        ],
        "elevation": 21.7
      },
      {
        "latitude": 54.402595,
        "longitude": 18.569027,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          34
        ],
        "elevation": 21.4
      },
      {
        "latitude": 54.4027,
        "longitude": 18.568958,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          38
        ],
        "elevation": 21.4
      },
      {
        "latitude": 54.4028,
        "longitude": 18.568887,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          42
        ],
        "elevation": 21.3
      },
      {
        "latitude": 54.402905,
        "longitude": 18.568837,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          46
        ],
        "elevation": 21.4
      },
      {
        "latitude": 54.403008,
        "longitude": 18.568763,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          50
        ],
        "elevation": 21.7
      },
      {
        "latitude": 54.403087,
        "longitude": 18.56864,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          54
        ],
        "elevation": 22
      },
      {
        "latitude": 54.403137,
        "longitude": 18.568475,
        "time": [
          2017,
          10,
          20,
          15,
          6,
          58
        ],
        "elevation": 22.7
      },
      {
        "latitude": 54.40315,
        "longitude": 18.568302,
        "time": [
          2017,
          10,
          20,
          15,
          7,
          2
        ],
        "elevation": 23.2
      },
      {
        "latitude": 54.403118,
        "longitude": 18.568117,
        "time": [
          2017,
          10,
          20,
          15,
          7,
          6
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.403072,
        "longitude": 18.567943,
        "time": [
          2017,
          10,
          20,
          15,
          7,
          10
        ],
        "elevation": 25.3
      },
      {
        "latitude": 54.402998,
        "longitude": 18.567807,
        "time": [
          2017,
          10,
          20,
          15,
          7,
          14
        ],
        "elevation": 25.6
      },
      {
        "latitude": 54.402902,
        "longitude": 18.567713,
        "time": [
          2017,
          10,
          20,
          15,
          7,
          18
        ],
        "elevation": 25
      },
      {
        "latitude": 54.402798,
        "longitude": 18.567698,
        "time": [
          2017,
          10,
          20,
          15,
          7,
          22
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.402685,
        "longitude": 18.567733,
        "time": [
          2017,
          10,
          20,
          15,
          7,
          26
        ],
        "elevation": 23.9
      },
      {
        "latitude": 54.402572,
        "longitude": 18.567788,
        "time": [
          2017,
          10,
          20,
          15,
          7,
          30
        ],
        "elevation": 24
      },
      {
        "latitude": 54.402463,
        "longitude": 18.567867,
        "time": [
          2017,
          10,
          20,
          15,
          7,
          34
        ],
        "elevation": 24.1
      },
      {
        "latitude": 54.40236,
        "longitude": 18.56795,
        "time": [
          2017,
          10,
          20,
          15,
          7,
          38
        ],
        "elevation": 24.1
      },
      {
        "latitude": 54.40227,
        "longitude": 18.568015,
        "time": [
          2017,
          10,
          20,
          15,
          7,
          41
        ],
        "elevation": 24.3
      },
      {
        "latitude": 54.402163,
        "longitude": 18.568093,
        "time": [
          2017,
          10,
          20,
          15,
          7,
          45
        ],
        "elevation": 25
      },
      {
        "latitude": 54.402088,
        "longitude": 18.568188,
        "time": [
          2017,
          10,
          20,
          15,
          7,
          49
        ],
        "elevation": 24.8
      },
      {
        "latitude": 54.402003,
        "longitude": 18.568287,
        "time": [
          2017,
          10,
          20,
          15,
          7,
          53
        ],
        "elevation": 24.7
      },
      {
        "latitude": 54.401913,
        "longitude": 18.56838,
        "time": [
          2017,
          10,
          20,
          15,
          7,
          57
        ],
        "elevation": 25
      },
      {
        "latitude": 54.401832,
        "longitude": 18.568488,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          1
        ],
        "elevation": 25.6
      },
      {
        "latitude": 54.401797,
        "longitude": 18.568633,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          5
        ],
        "elevation": 25.7
      },
      {
        "latitude": 54.401807,
        "longitude": 18.568812,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          9
        ],
        "elevation": 25
      },
      {
        "latitude": 54.401855,
        "longitude": 18.568977,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          13
        ],
        "elevation": 24.6
      },
      {
        "latitude": 54.401922,
        "longitude": 18.569118,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          18
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.40201,
        "longitude": 18.569198,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          22
        ],
        "elevation": 24
      },
      {
        "latitude": 54.402102,
        "longitude": 18.56923,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          26
        ],
        "elevation": 23.3
      },
      {
        "latitude": 54.402193,
        "longitude": 18.569203,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          30
        ],
        "elevation": 22.9
      },
      {
        "latitude": 54.402295,
        "longitude": 18.569205,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          33
        ],
        "elevation": 22.4
      },
      {
        "latitude": 54.402393,
        "longitude": 18.569142,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          37
        ],
        "elevation": 22
      },
      {
        "latitude": 54.402485,
        "longitude": 18.569098,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          40
        ],
        "elevation": 21.7
      },
      {
        "latitude": 54.402592,
        "longitude": 18.569103,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          42
        ],
        "elevation": 21.5
      },
      {
        "latitude": 54.402695,
        "longitude": 18.569055,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          45
        ],
        "elevation": 21.4
      },
      {
        "latitude": 54.402792,
        "longitude": 18.568968,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          49
        ],
        "elevation": 21.3
      },
      {
        "latitude": 54.402898,
        "longitude": 18.568873,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          53
        ],
        "elevation": 21.4
      },
      {
        "latitude": 54.403007,
        "longitude": 18.5688,
        "time": [
          2017,
          10,
          20,
          15,
          8,
          57
        ],
        "elevation": 21.7
      },
      {
        "latitude": 54.403085,
        "longitude": 18.568667,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          2
        ],
        "elevation": 22
      },
      {
        "latitude": 54.403125,
        "longitude": 18.568515,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          6
        ],
        "elevation": 22.6
      },
      {
        "latitude": 54.403145,
        "longitude": 18.56835,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          10
        ],
        "elevation": 22.9
      },
      {
        "latitude": 54.403128,
        "longitude": 18.568143,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          14
        ],
        "elevation": 23.7
      },
      {
        "latitude": 54.4031,
        "longitude": 18.567995,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          17
        ],
        "elevation": 24.6
      },
      {
        "latitude": 54.403028,
        "longitude": 18.567845,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          21
        ],
        "elevation": 25
      },
      {
        "latitude": 54.402957,
        "longitude": 18.567733,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          24
        ],
        "elevation": 24.8
      },
      {
        "latitude": 54.402855,
        "longitude": 18.567677,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          28
        ],
        "elevation": 24.1
      },
      {
        "latitude": 54.402747,
        "longitude": 18.567682,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          32
        ],
        "elevation": 23.5
      },
      {
        "latitude": 54.40266,
        "longitude": 18.567723,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          35
        ],
        "elevation": 23.3
      },
      {
        "latitude": 54.402552,
        "longitude": 18.5678,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          39
        ],
        "elevation": 23.3
      },
      {
        "latitude": 54.402438,
        "longitude": 18.567873,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          43
        ],
        "elevation": 23.4
      },
      {
        "latitude": 54.40233,
        "longitude": 18.567953,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          47
        ],
        "elevation": 23.4
      },
      {
        "latitude": 54.402247,
        "longitude": 18.568012,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          50
        ],
        "elevation": 23.5
      },
      {
        "latitude": 54.402153,
        "longitude": 18.568092,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          54
        ],
        "elevation": 24.1
      },
      {
        "latitude": 54.402065,
        "longitude": 18.56817,
        "time": [
          2017,
          10,
          20,
          15,
          9,
          58
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.40198,
        "longitude": 18.568225,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          1
        ],
        "elevation": 24.1
      },
      {
        "latitude": 54.401885,
        "longitude": 18.56832,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          5
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.401825,
        "longitude": 18.568458,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          9
        ],
        "elevation": 24.7
      },
      {
        "latitude": 54.401797,
        "longitude": 18.56864,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          13
        ],
        "elevation": 24.7
      },
      {
        "latitude": 54.401795,
        "longitude": 18.568808,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          17
        ],
        "elevation": 24
      },
      {
        "latitude": 54.401795,
        "longitude": 18.568967,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          21
        ],
        "elevation": 23.8
      },
      {
        "latitude": 54.401862,
        "longitude": 18.569122,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          25
        ],
        "elevation": 23.7
      },
      {
        "latitude": 54.401963,
        "longitude": 18.569243,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          26
        ],
        "elevation": 23
      },
      {
        "latitude": 54.402052,
        "longitude": 18.569343,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          29
        ],
        "elevation": 23
      },
      {
        "latitude": 54.40214,
        "longitude": 18.569377,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          33
        ],
        "elevation": 23
      },
      {
        "latitude": 54.402232,
        "longitude": 18.569372,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          36
        ],
        "elevation": 22.9
      },
      {
        "latitude": 54.402338,
        "longitude": 18.569315,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          40
        ],
        "elevation": 22.4
      },
      {
        "latitude": 54.402427,
        "longitude": 18.569258,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          43
        ],
        "elevation": 22
      },
      {
        "latitude": 54.40252,
        "longitude": 18.569212,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          46
        ],
        "elevation": 21.7
      },
      {
        "latitude": 54.402618,
        "longitude": 18.569123,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          50
        ],
        "elevation": 21.4
      },
      {
        "latitude": 54.402717,
        "longitude": 18.56903,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          54
        ],
        "elevation": 21.4
      },
      {
        "latitude": 54.40281,
        "longitude": 18.568913,
        "time": [
          2017,
          10,
          20,
          15,
          10,
          58
        ],
        "elevation": 21.3
      },
      {
        "latitude": 54.402915,
        "longitude": 18.568822,
        "time": [
          2017,
          10,
          20,
          15,
          11,
          2
        ],
        "elevation": 21.5
      },
      {
        "latitude": 54.403002,
        "longitude": 18.568713,
        "time": [
          2017,
          10,
          20,
          15,
          11,
          6
        ],
        "elevation": 21.7
      },
      {
        "latitude": 54.403068,
        "longitude": 18.568572,
        "time": [
          2017,
          10,
          20,
          15,
          11,
          10
        ],
        "elevation": 22.2
      },
      {
        "latitude": 54.403123,
        "longitude": 18.56842,
        "time": [
          2017,
          10,
          20,
          15,
          11,
          14
        ],
        "elevation": 22.8
      },
      {
        "latitude": 54.40314,
        "longitude": 18.568267,
        "time": [
          2017,
          10,
          20,
          15,
          11,
          18
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.403128,
        "longitude": 18.568093,
        "time": [
          2017,
          10,
          20,
          15,
          11,
          22
        ],
        "elevation": 23.8
      },
      {
        "latitude": 54.40308,
        "longitude": 18.56794,
        "time": [
          2017,
          10,
          20,
          15,
          11,
          26
        ],
        "elevation": 24.7
      },
      {
        "latitude": 54.403,
        "longitude": 18.567805,
        "time": [
          2017,
          10,
          20,
          15,
          11,
          30
        ],
        "elevation": 25
      },
      {
        "latitude": 54.402898,
        "longitude": 18.567707,
        "time": [
          2017,
          10,
          20,
          15,
          11,
          34
        ],
        "elevation": 24.3
      },
      {
        "latitude": 54.402792,
        "longitude": 18.56768,
        "time": [
          2017,
          10,
          20,
          15,
          11,
          38
        ],
        "elevation": 23.4
      },
      {
        "latitude": 54.402675,
        "longitude": 18.567725,
        "time": [
          2017,
          10,
          20,
          15,
          11,
          42
        ],
        "elevation": 23
      },
      {
        "latitude": 54.402565,
        "longitude": 18.567802,
        "time": [
          2017,
          10,
          20,
          15,
          11,
          46
        ],
        "elevation": 22.9
      },
      {
        "latitude": 54.402458,
        "longitude": 18.567868,
        "time": [
          2017,
          10,
          20,
          15,
          11,
          50
        ],
        "elevation": 23
      },
      {
        "latitude": 54.40236,
        "longitude": 18.567945,
        "time": [
          2017,
          10,
          20,
          15,
          11,
          54
        ],
        "elevation": 22.9
      },
      {
        "latitude": 54.402268,
        "longitude": 18.567987,
        "time": [
          2017,
          10,
          20,
          15,
          11,
          57
        ],
        "elevation": 23.2
      },
      {
        "latitude": 54.402162,
        "longitude": 18.568068,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          1
        ],
        "elevation": 23.6
      },
      {
        "latitude": 54.402078,
        "longitude": 18.568142,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          4
        ],
        "elevation": 23.6
      },
      {
        "latitude": 54.401973,
        "longitude": 18.568225,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          8
        ],
        "elevation": 23.5
      },
      {
        "latitude": 54.401882,
        "longitude": 18.568328,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          12
        ],
        "elevation": 23.7
      },
      {
        "latitude": 54.401793,
        "longitude": 18.568442,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          16
        ],
        "elevation": 24.3
      },
      {
        "latitude": 54.401772,
        "longitude": 18.56861,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          20
        ],
        "elevation": 24
      },
      {
        "latitude": 54.401782,
        "longitude": 18.568792,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          24
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.401802,
        "longitude": 18.56895,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          28
        ],
        "elevation": 22.8
      },
      {
        "latitude": 54.401858,
        "longitude": 18.569093,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          32
        ],
        "elevation": 22.7
      },
      {
        "latitude": 54.401947,
        "longitude": 18.569208,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          36
        ],
        "elevation": 22.1
      },
      {
        "latitude": 54.40203,
        "longitude": 18.569268,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          39
        ],
        "elevation": 21.4
      },
      {
        "latitude": 54.402143,
        "longitude": 18.569272,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          43
        ],
        "elevation": 20.9
      },
      {
        "latitude": 54.402235,
        "longitude": 18.56924,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          46
        ],
        "elevation": 20.4
      },
      {
        "latitude": 54.402332,
        "longitude": 18.569208,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          49
        ],
        "elevation": 20
      },
      {
        "latitude": 54.402428,
        "longitude": 18.569168,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          52
        ],
        "elevation": 20
      },
      {
        "latitude": 54.402532,
        "longitude": 18.569087,
        "time": [
          2017,
          10,
          20,
          15,
          12,
          56
        ],
        "elevation": 20.2
      },
      {
        "latitude": 54.402637,
        "longitude": 18.569003,
        "time": [
          2017,
          10,
          20,
          15,
          13
        ],
        "elevation": 20.5
      },
      {
        "latitude": 54.402728,
        "longitude": 18.568905,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          4
        ],
        "elevation": 21.1
      },
      {
        "latitude": 54.402835,
        "longitude": 18.56881,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          8
        ],
        "elevation": 21.8
      },
      {
        "latitude": 54.402927,
        "longitude": 18.568738,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          11
        ],
        "elevation": 22.6
      },
      {
        "latitude": 54.403007,
        "longitude": 18.568667,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          14
        ],
        "elevation": 23
      },
      {
        "latitude": 54.403077,
        "longitude": 18.56852,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          18
        ],
        "elevation": 23.3
      },
      {
        "latitude": 54.403108,
        "longitude": 18.568368,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          22
        ],
        "elevation": 23.2
      },
      {
        "latitude": 54.403107,
        "longitude": 18.568185,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          26
        ],
        "elevation": 23.6
      },
      {
        "latitude": 54.403085,
        "longitude": 18.568027,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          30
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.403033,
        "longitude": 18.567893,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          34
        ],
        "elevation": 24.8
      },
      {
        "latitude": 54.402945,
        "longitude": 18.567782,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          38
        ],
        "elevation": 24.5
      },
      {
        "latitude": 54.402837,
        "longitude": 18.567727,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          42
        ],
        "elevation": 23.7
      },
      {
        "latitude": 54.402725,
        "longitude": 18.567747,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          46
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.40263,
        "longitude": 18.567787,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          49
        ],
        "elevation": 22.9
      },
      {
        "latitude": 54.402542,
        "longitude": 18.567835,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          52
        ],
        "elevation": 23
      },
      {
        "latitude": 54.402457,
        "longitude": 18.5679,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          55
        ],
        "elevation": 22.9
      },
      {
        "latitude": 54.402372,
        "longitude": 18.567953,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          58
        ],
        "elevation": 22.8
      },
      {
        "latitude": 54.402278,
        "longitude": 18.567997,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          1
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.402183,
        "longitude": 18.568048,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          4
        ],
        "elevation": 23.6
      },
      {
        "latitude": 54.402093,
        "longitude": 18.568098,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          7
        ],
        "elevation": 23.9
      },
      {
        "latitude": 54.402008,
        "longitude": 18.568165,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          10
        ],
        "elevation": 24.1
      },
      {
        "latitude": 54.401905,
        "longitude": 18.568243,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          14
        ],
        "elevation": 24
      },
      {
        "latitude": 54.401812,
        "longitude": 18.568353,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          18
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.401762,
        "longitude": 18.568495,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          22
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.401747,
        "longitude": 18.5687,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          26
        ],
        "elevation": 23.6
      },
      {
        "latitude": 54.401762,
        "longitude": 18.568858,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          29
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.401815,
        "longitude": 18.569023,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          33
        ],
        "elevation": 22.9
      },
      {
        "latitude": 54.401898,
        "longitude": 18.569162,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          38
        ],
        "elevation": 22.5
      },
      {
        "latitude": 54.402,
        "longitude": 18.569237,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          42
        ],
        "elevation": 21.7
      },
      {
        "latitude": 54.402103,
        "longitude": 18.569225,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          46
        ],
        "elevation": 21.1
      },
      {
        "latitude": 54.402192,
        "longitude": 18.569182,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          49
        ],
        "elevation": 20.5
      },
      {
        "latitude": 54.402275,
        "longitude": 18.569108,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          52
        ],
        "elevation": 19.9
      },
      {
        "latitude": 54.402355,
        "longitude": 18.569032,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          55
        ],
        "elevation": 19.8
      },
      {
        "latitude": 54.402442,
        "longitude": 18.568968,
        "time": [
          2017,
          10,
          20,
          15,
          14,
          58
        ],
        "elevation": 20
      },
      {
        "latitude": 54.402602,
        "longitude": 18.56898,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          1
        ],
        "elevation": 20.5
      },
      {
        "latitude": 54.402707,
        "longitude": 18.568942,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          4
        ],
        "elevation": 20.9
      },
      {
        "latitude": 54.402812,
        "longitude": 18.568892,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          7
        ],
        "elevation": 21.4
      },
      {
        "latitude": 54.4029,
        "longitude": 18.568818,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          10
        ],
        "elevation": 22.1
      },
      {
        "latitude": 54.40299,
        "longitude": 18.568747,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          13
        ],
        "elevation": 22.6
      },
      {
        "latitude": 54.403072,
        "longitude": 18.568653,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          16
        ],
        "elevation": 23.2
      },
      {
        "latitude": 54.403128,
        "longitude": 18.568482,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          20
        ],
        "elevation": 23.2
      },
      {
        "latitude": 54.40315,
        "longitude": 18.568285,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          24
        ],
        "elevation": 23.4
      },
      {
        "latitude": 54.403133,
        "longitude": 18.568122,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          27
        ],
        "elevation": 24.1
      },
      {
        "latitude": 54.40309,
        "longitude": 18.567985,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          30
        ],
        "elevation": 24.6
      },
      {
        "latitude": 54.403022,
        "longitude": 18.567878,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          33
        ],
        "elevation": 25
      },
      {
        "latitude": 54.40293,
        "longitude": 18.567782,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          36
        ],
        "elevation": 24.6
      },
      {
        "latitude": 54.402835,
        "longitude": 18.567757,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          39
        ],
        "elevation": 23.7
      },
      {
        "latitude": 54.402737,
        "longitude": 18.56777,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          42
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.402637,
        "longitude": 18.567813,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          45
        ],
        "elevation": 22.9
      },
      {
        "latitude": 54.402525,
        "longitude": 18.567872,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          48
        ],
        "elevation": 22.9
      },
      {
        "latitude": 54.402423,
        "longitude": 18.567928,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          51
        ],
        "elevation": 22.8
      },
      {
        "latitude": 54.402327,
        "longitude": 18.567997,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          54
        ],
        "elevation": 22.9
      },
      {
        "latitude": 54.402227,
        "longitude": 18.568038,
        "time": [
          2017,
          10,
          20,
          15,
          15,
          57
        ],
        "elevation": 23.3
      },
      {
        "latitude": 54.402128,
        "longitude": 18.568093,
        "time": [
          2017,
          10,
          20,
          15,
          16
        ],
        "elevation": 23.8
      },
      {
        "latitude": 54.402025,
        "longitude": 18.568175,
        "time": [
          2017,
          10,
          20,
          15,
          16,
          3
        ],
        "elevation": 24
      },
      {
        "latitude": 54.40194,
        "longitude": 18.568252,
        "time": [
          2017,
          10,
          20,
          15,
          16,
          7
        ],
        "elevation": 23.8
      },
      {
        "latitude": 54.40203,
        "longitude": 18.56826,
        "time": [
          2017,
          10,
          20,
          15,
          18,
          32
        ],
        "elevation": 23.3
      },
      {
        "latitude": 54.402102,
        "longitude": 18.568143,
        "time": [
          2017,
          10,
          20,
          15,
          19,
          36
        ],
        "elevation": 23.6
      },
      {
        "latitude": 54.402197,
        "longitude": 18.568055,
        "time": [
          2017,
          10,
          20,
          15,
          19,
          39
        ],
        "elevation": 23.3
      },
      {
        "latitude": 54.40229,
        "longitude": 18.56799,
        "time": [
          2017,
          10,
          20,
          15,
          19,
          42
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.40238,
        "longitude": 18.567922,
        "time": [
          2017,
          10,
          20,
          15,
          19,
          45
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.402468,
        "longitude": 18.567845,
        "time": [
          2017,
          10,
          20,
          15,
          19,
          48
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.402553,
        "longitude": 18.56776,
        "time": [
          2017,
          10,
          20,
          15,
          19,
          51
        ],
        "elevation": 23.2
      },
      {
        "latitude": 54.402637,
        "longitude": 18.567692,
        "time": [
          2017,
          10,
          20,
          15,
          19,
          54
        ],
        "elevation": 23.4
      },
      {
        "latitude": 54.402723,
        "longitude": 18.567635,
        "time": [
          2017,
          10,
          20,
          15,
          19,
          57
        ],
        "elevation": 23.5
      },
      {
        "latitude": 54.40281,
        "longitude": 18.567593,
        "time": [
          2017,
          10,
          20,
          15,
          20
        ],
        "elevation": 23.9
      },
      {
        "latitude": 54.402907,
        "longitude": 18.567613,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          4
        ],
        "elevation": 24.5
      },
      {
        "latitude": 54.402997,
        "longitude": 18.567705,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          8
        ],
        "elevation": 25.2
      },
      {
        "latitude": 54.403058,
        "longitude": 18.567823,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          11
        ],
        "elevation": 25.2
      },
      {
        "latitude": 54.40311,
        "longitude": 18.56797,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          14
        ],
        "elevation": 24.7
      },
      {
        "latitude": 54.403133,
        "longitude": 18.568142,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          17
        ],
        "elevation": 23.7
      },
      {
        "latitude": 54.403135,
        "longitude": 18.568313,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          20
        ],
        "elevation": 23
      },
      {
        "latitude": 54.403112,
        "longitude": 18.56847,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          23
        ],
        "elevation": 22.7
      },
      {
        "latitude": 54.403062,
        "longitude": 18.568638,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          27
        ],
        "elevation": 22
      },
      {
        "latitude": 54.403017,
        "longitude": 18.568778,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          30
        ],
        "elevation": 22
      },
      {
        "latitude": 54.40296,
        "longitude": 18.56894,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          34
        ],
        "elevation": 21.7
      },
      {
        "latitude": 54.402897,
        "longitude": 18.569075,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          38
        ],
        "elevation": 21.8
      },
      {
        "latitude": 54.402873,
        "longitude": 18.569245,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          45
        ],
        "elevation": 22
      },
      {
        "latitude": 54.402867,
        "longitude": 18.56941,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          50
        ],
        "elevation": 22.1
      },
      {
        "latitude": 54.402798,
        "longitude": 18.569527,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          55
        ],
        "elevation": 22.1
      },
      {
        "latitude": 54.402703,
        "longitude": 18.569523,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          58
        ],
        "elevation": 22.2
      },
      {
        "latitude": 54.402603,
        "longitude": 18.569552,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          1
        ],
        "elevation": 22.2
      },
      {
        "latitude": 54.402498,
        "longitude": 18.569643,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          5
        ],
        "elevation": 22.3
      },
      {
        "latitude": 54.402392,
        "longitude": 18.569733,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          9
        ],
        "elevation": 22.4
      },
      {
        "latitude": 54.402298,
        "longitude": 18.569792,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          13
        ],
        "elevation": 22.6
      },
      {
        "latitude": 54.402315,
        "longitude": 18.570017,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          17
        ],
        "elevation": 22.9
      },
      {
        "latitude": 54.40235,
        "longitude": 18.570167,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          19
        ],
        "elevation": 22.3
      },
      {
        "latitude": 54.402375,
        "longitude": 18.570357,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          22
        ],
        "elevation": 21
      },
      {
        "latitude": 54.402417,
        "longitude": 18.570535,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          26
        ],
        "elevation": 21
      },
      {
        "latitude": 54.402425,
        "longitude": 18.570698,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          29
        ],
        "elevation": 21
      },
      {
        "latitude": 54.402447,
        "longitude": 18.570862,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          32
        ],
        "elevation": 21
      },
      {
        "latitude": 54.402482,
        "longitude": 18.571055,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          36
        ],
        "elevation": 21
      },
      {
        "latitude": 54.402497,
        "longitude": 18.57122,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          39
        ],
        "elevation": 20.6
      },
      {
        "latitude": 54.402528,
        "longitude": 18.571408,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          43
        ],
        "elevation": 20.4
      },
      {
        "latitude": 54.402567,
        "longitude": 18.571592,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          47
        ],
        "elevation": 20.3
      },
      {
        "latitude": 54.402592,
        "longitude": 18.57176,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          51
        ],
        "elevation": 20.2
      },
      {
        "latitude": 54.40254,
        "longitude": 18.571908,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          59
        ],
        "elevation": 20.1
      },
      {
        "latitude": 54.402515,
        "longitude": 18.57208,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          5
        ],
        "elevation": 20
      },
      {
        "latitude": 54.402437,
        "longitude": 18.572187,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          8
        ],
        "elevation": 20
      },
      {
        "latitude": 54.402362,
        "longitude": 18.572277,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          11
        ],
        "elevation": 20
      },
      {
        "latitude": 54.402285,
        "longitude": 18.572405,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          15
        ],
        "elevation": 20.7
      },
      {
        "latitude": 54.402198,
        "longitude": 18.572505,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          19
        ],
        "elevation": 20.8
      },
      {
        "latitude": 54.402118,
        "longitude": 18.572598,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          23
        ],
        "elevation": 21
      },
      {
        "latitude": 54.402022,
        "longitude": 18.572698,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          27
        ],
        "elevation": 21
      },
      {
        "latitude": 54.40195,
        "longitude": 18.572793,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          30
        ],
        "elevation": 21
      },
      {
        "latitude": 54.401865,
        "longitude": 18.572868,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          33
        ],
        "elevation": 21.3
      },
      {
        "latitude": 54.40177,
        "longitude": 18.572937,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          36
        ],
        "elevation": 22
      },
      {
        "latitude": 54.401675,
        "longitude": 18.573015,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          39
        ],
        "elevation": 22.5
      },
      {
        "latitude": 54.401583,
        "longitude": 18.573078,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          42
        ],
        "elevation": 22.5
      },
      {
        "latitude": 54.401497,
        "longitude": 18.573163,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          45
        ],
        "elevation": 22
      },
      {
        "latitude": 54.401423,
        "longitude": 18.573258,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          48
        ],
        "elevation": 21.4
      },
      {
        "latitude": 54.401348,
        "longitude": 18.573345,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          51
        ],
        "elevation": 21.1
      },
      {
        "latitude": 54.401253,
        "longitude": 18.573453,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          55
        ],
        "elevation": 20.6
      },
      {
        "latitude": 54.401172,
        "longitude": 18.573567,
        "time": [
          2017,
          10,
          20,
          15,
          22,
          59
        ],
        "elevation": 20.1
      },
      {
        "latitude": 54.4011,
        "longitude": 18.573668,
        "time": [
          2017,
          10,
          20,
          15,
          23,
          7
        ],
        "elevation": 20.1
      },
      {
        "latitude": 54.401143,
        "longitude": 18.573835,
        "time": [
          2017,
          10,
          20,
          15,
          23,
          28
        ],
        "elevation": 20
      },
      {
        "latitude": 54.40119,
        "longitude": 18.573992,
        "time": [
          2017,
          10,
          20,
          15,
          23,
          31
        ],
        "elevation": 19.4
      },
      {
        "latitude": 54.401223,
        "longitude": 18.574143,
        "time": [
          2017,
          10,
          20,
          15,
          23,
          34
        ],
        "elevation": 19
      },
      {
        "latitude": 54.40122,
        "longitude": 18.574303,
        "time": [
          2017,
          10,
          20,
          15,
          23,
          37
        ],
        "elevation": 19
      },
      {
        "latitude": 54.401227,
        "longitude": 18.574515,
        "time": [
          2017,
          10,
          20,
          15,
          23,
          40
        ],
        "elevation": 18.8
      },
      {
        "latitude": 54.401262,
        "longitude": 18.574677,
        "time": [
          2017,
          10,
          20,
          15,
          23,
          42
        ],
        "elevation": 18.6
      },
      {
        "latitude": 54.401298,
        "longitude": 18.574845,
        "time": [
          2017,
          10,
          20,
          15,
          23,
          44
        ],
        "elevation": 18.3
      },
      {
        "latitude": 54.40133,
        "longitude": 18.575037,
        "time": [
          2017,
          10,
          20,
          15,
          23,
          46
        ],
        "elevation": 17.9
      },
      {
        "latitude": 54.401353,
        "longitude": 18.57524,
        "time": [
          2017,
          10,
          20,
          15,
          23,
          48
        ],
        "elevation": 17.5
      },
      {
        "latitude": 54.401373,
        "longitude": 18.575432,
        "time": [
          2017,
          10,
          20,
          15,
          23,
          50
        ],
        "elevation": 17.1
      },
      {
        "latitude": 54.401435,
        "longitude": 18.575612,
        "time": [
          2017,
          10,
          20,
          15,
          23,
          53
        ],
        "elevation": 17.3
      },
      {
        "latitude": 54.40152,
        "longitude": 18.575723,
        "time": [
          2017,
          10,
          20,
          15,
          23,
          56
        ],
        "elevation": 17.5
      },
      {
        "latitude": 54.401547,
        "longitude": 18.575927,
        "time": [
          2017,
          10,
          20,
          15,
          24
        ],
        "elevation": 17.7
      },
      {
        "latitude": 54.401558,
        "longitude": 18.576082,
        "time": [
          2017,
          10,
          20,
          15,
          24,
          3
        ],
        "elevation": 17.8
      },
      {
        "latitude": 54.401585,
        "longitude": 18.576237,
        "time": [
          2017,
          10,
          20,
          15,
          24,
          6
        ],
        "elevation": 18
      },
      {
        "latitude": 54.40166,
        "longitude": 18.576328,
        "time": [
          2017,
          10,
          20,
          15,
          24,
          9
        ],
        "elevation": 18
      },
      {
        "latitude": 54.401765,
        "longitude": 18.576378,
        "time": [
          2017,
          10,
          20,
          15,
          24,
          13
        ],
        "elevation": 18
      },
      {
        "latitude": 54.401877,
        "longitude": 18.576338,
        "time": [
          2017,
          10,
          20,
          15,
          24,
          17
        ],
        "elevation": 18
      },
      {
        "latitude": 54.401967,
        "longitude": 18.576307,
        "time": [
          2017,
          10,
          20,
          15,
          24,
          21
        ],
        "elevation": 18
      },
      {
        "latitude": 54.402047,
        "longitude": 18.576425,
        "time": [
          2017,
          10,
          20,
          15,
          24,
          25
        ],
        "elevation": 18
      },
      {
        "latitude": 54.402102,
        "longitude": 18.576557,
        "time": [
          2017,
          10,
          20,
          15,
          24,
          28
        ],
        "elevation": 17.8
      },
      {
        "latitude": 54.402165,
        "longitude": 18.576712,
        "time": [
          2017,
          10,
          20,
          15,
          24,
          32
        ],
        "elevation": 17.8
      },
      {
        "latitude": 54.402223,
        "longitude": 18.576883,
        "time": [
          2017,
          10,
          20,
          15,
          24,
          36
        ],
        "elevation": 17.4
      },
      {
        "latitude": 54.402292,
        "longitude": 18.577018,
        "time": [
          2017,
          10,
          20,
          15,
          24,
          40
        ],
        "elevation": 17
      },
      {
        "latitude": 54.402383,
        "longitude": 18.577022,
        "time": [
          2017,
          10,
          20,
          15,
          24,
          43
        ],
        "elevation": 17
      },
      {
        "latitude": 54.402488,
        "longitude": 18.576978,
        "time": [
          2017,
          10,
          20,
          15,
          24,
          47
        ],
        "elevation": 17.4
      },
      {
        "latitude": 54.402597,
        "longitude": 18.576933,
        "time": [
          2017,
          10,
          20,
          15,
          24,
          51
        ],
        "elevation": 18.2
      },
      {
        "latitude": 54.402703,
        "longitude": 18.57689,
        "time": [
          2017,
          10,
          20,
          15,
          24,
          55
        ],
        "elevation": 18.8
      },
      {
        "latitude": 54.402805,
        "longitude": 18.576945,
        "time": [
          2017,
          10,
          20,
          15,
          25,
          1
        ],
        "elevation": 19.1
      },
      {
        "latitude": 54.402837,
        "longitude": 18.577107,
        "time": [
          2017,
          10,
          20,
          15,
          25,
          5
        ],
        "elevation": 18.8
      },
      {
        "latitude": 54.402923,
        "longitude": 18.57718,
        "time": [
          2017,
          10,
          20,
          15,
          25,
          10
        ],
        "elevation": 18.3
      },
      {
        "latitude": 54.40302,
        "longitude": 18.57718,
        "time": [
          2017,
          10,
          20,
          15,
          25,
          18
        ],
        "elevation": 18.4
      },
      {
        "latitude": 54.403123,
        "longitude": 18.577122,
        "time": [
          2017,
          10,
          20,
          15,
          25,
          22
        ],
        "elevation": 19
      },
      {
        "latitude": 54.4032,
        "longitude": 18.57701,
        "time": [
          2017,
          10,
          20,
          15,
          25,
          27
        ],
        "elevation": 19.2
      },
      {
        "latitude": 54.403293,
        "longitude": 18.576958,
        "time": [
          2017,
          10,
          20,
          15,
          25,
          31
        ],
        "elevation": 19.5
      },
      {
        "latitude": 54.403392,
        "longitude": 18.576973,
        "time": [
          2017,
          10,
          20,
          15,
          25,
          34
        ],
        "elevation": 19.4
      },
      {
        "latitude": 54.4035,
        "longitude": 18.576985,
        "time": [
          2017,
          10,
          20,
          15,
          25,
          36
        ],
        "elevation": 19.2
      }
    ]
  }

  export const numberFiveRelatives = {
    "6": [
      {
        "latitude": 54.403605,
        "longitude": 18.577163,
        "time": [
          2017,
          10,
          20,
          14,
          49,
          23
        ],
        "elevation": 19.3
      },
      {
        "latitude": 54.403497,
        "longitude": 18.57715,
        "time": [
          2017,
          10,
          20,
          14,
          49,
          32
        ],
        "elevation": 19.4
      },
      {
        "latitude": 54.403383,
        "longitude": 18.577215,
        "time": [
          2017,
          10,
          20,
          14,
          49,
          36
        ],
        "elevation": 19.5
      },
      {
        "latitude": 54.403295,
        "longitude": 18.57728,
        "time": [
          2017,
          10,
          20,
          14,
          49,
          39
        ],
        "elevation": 19.4
      },
      {
        "latitude": 54.403208,
        "longitude": 18.577325,
        "time": [
          2017,
          10,
          20,
          14,
          49,
          42
        ],
        "elevation": 19.3
      },
      {
        "latitude": 54.403108,
        "longitude": 18.577257,
        "time": [
          2017,
          10,
          20,
          14,
          49,
          46
        ],
        "elevation": 19
      },


      
      {
        "latitude": 54.402822,
        "longitude": 18.567705,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          11
        ],
        "elevation": 25
      },
      {
        "latitude": 54.402708,
        "longitude": 18.567725,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          15
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.40261,
        "longitude": 18.56776,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          18
        ],
        "elevation": 24.3
      },
      {
        "latitude": 54.40252,
        "longitude": 18.567803,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          21
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.402413,
        "longitude": 18.56788,
        "time": [
          2017,
          10,
          20,
          15,
          5,
          25
        ],
        "elevation": 24.4
      },
    ],
      


    "7": [
      {
        "latitude": 54.403107,
        "longitude": 18.568185,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          26
        ],
        "elevation": 23.6
      },
      {
        "latitude": 54.403085,
        "longitude": 18.568027,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          30
        ],
        "elevation": 24.4
      },
      {
        "latitude": 54.403033,
        "longitude": 18.567893,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          34
        ],
        "elevation": 24.8
      },
      {
        "latitude": 54.402945,
        "longitude": 18.567782,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          38
        ],
        "elevation": 24.5
      },
      {
        "latitude": 54.402837,
        "longitude": 18.567727,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          42
        ],
        "elevation": 23.7
      },
      {
        "latitude": 54.402725,
        "longitude": 18.567747,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          46
        ],
        "elevation": 23.1
      },
      {
        "latitude": 54.40263,
        "longitude": 18.567787,
        "time": [
          2017,
          10,
          20,
          15,
          13,
          49
        ],
        "elevation": 22.9
      },




      {
        "latitude": 54.402867,
        "longitude": 18.56941,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          50
        ],
        "elevation": 22.1
      },
      {
        "latitude": 54.402798,
        "longitude": 18.569527,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          55
        ],
        "elevation": 22.1
      },
      {
        "latitude": 54.402703,
        "longitude": 18.569523,
        "time": [
          2017,
          10,
          20,
          15,
          20,
          58
        ],
        "elevation": 22.2
      },
      {
        "latitude": 54.402603,
        "longitude": 18.569552,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          1
        ],
        "elevation": 22.2
      },
      {
        "latitude": 54.402498,
        "longitude": 18.569643,
        "time": [
          2017,
          10,
          20,
          15,
          21,
          5
        ],
        "elevation": 22.3
      },
      




      {
        "latitude": 54.403123,
        "longitude": 18.577122,
        "time": [
          2017,
          10,
          20,
          15,
          25,
          22
        ],
        "elevation": 19
      },
      {
        "latitude": 54.4032,
        "longitude": 18.57701,
        "time": [
          2017,
          10,
          20,
          15,
          25,
          27
        ],
        "elevation": 19.2
      },
      {
        "latitude": 54.403293,
        "longitude": 18.576958,
        "time": [
          2017,
          10,
          20,
          15,
          25,
          31
        ],
        "elevation": 19.5
      },
      {
        "latitude": 54.403392,
        "longitude": 18.576973,
        "time": [
          2017,
          10,
          20,
          15,
          25,
          34
        ],
        "elevation": 19.4
      },
      {
        "latitude": 54.4035,
        "longitude": 18.576985,
        "time": [
          2017,
          10,
          20,
          15,
          25,
          36
        ],
        "elevation": 19.2
      }
    ]
  }