import { numberFive } from '../mocks/tracks'
import { mockMap } from './faker'

const serverAddress = 'http://85.222.94.122:8888'
const fakerActivated = false

const request = (body = null, method = 'GET', credentials = null, contentType = 'application/json') => {
    const headers = new Headers({})
    if (contentType === 'application/json') {
        headers.append('Content-Type', contentType)
    }
    const mockCredentials = sessionStorage.auth === undefined ? {} : JSON.parse(sessionStorage.auth)
    const { username, password } = credentials === null ? mockCredentials : credentials
    if (username !== undefined && password !== undefined) {
        headers.append('Authorization', 'Basic ' + btoa(username + ":" + password));
    }

    let request = {
        method,
        headers,
        modes: 'cors'
    }

    if (body === null) {
        return request
    } else {
        if (contentType === 'application/json') {
            body = JSON.stringify(body)
        }
        return {
            ...request,
            body
        }
    }
}

const injectMock = (request, onSuccess, onFail) => {
    const { path, body } = request
    const response = mockMap[path]()
    onSuccess(response)
}

export const get = (
    path,
    onSuccess,
    onFail = (error) => {
        alert('Problem przy wczytywaniu')
    },
    credentials = null
) => {

    if (fakerActivated) {
        injectMock({ path }, onSuccess, onFail)
        return
    }

    let realPath = serverAddress + path
    fetch(
        realPath,
        request(null, 'GET', credentials)
    )
        .then(response => {
            if (response.ok) {
                return response.json()
            } else {
                throw new Error('Bad response ' + response.url + ' ' + response.status);
            }
        })
        .then(json => { onSuccess(json) })
        .catch(error => {
            onFail(error)
        })
}

export const post = (
    path,
    body,
    onSuccess,
    onFail = (error) => { alert('Problem przy wczytywaniu') },
    credentials = null,
    contentType = 'application/json'
) => {

    if (fakerActivated) {
        // Post requests hadling in faker
        return
    }

    let realPath = serverAddress + path
    fetch(
        realPath,
        request(body, 'POST', credentials, contentType)
    )
        .then(response => {
            if (response.ok) {
                return response.json()
            } else {
                throw new Error('Bad response ' + response.url + ' ' + response.status);
            }
        })
        .then(json => { onSuccess(json) })
        .catch(error => {
            onFail(error)
        })
}