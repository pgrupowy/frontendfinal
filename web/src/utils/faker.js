import { index, getUserByTrackId, numberFive, numberFiveRelatives } from '../mocks/tracks'

const availablPaths = index.reduce((obj, track) => {
    return {
        ...obj,
        ['/rest/tracks/' + track.id]: () => ({
            ...numberFive,
            id: track.id
        }),
        ['/rest/tracks/' + track.id + '/relatives' ]: () => numberFiveRelatives,
        ['/rest/tracks/' + track.id + '/user']: () => getUserByTrackId,
    }
}, {})

const staticMap = {
    '/rest/login': () => { },
    '/rest/tracks': () => index,
}

export const mockMap = {
    ...staticMap,
    ...availablPaths
}