export const swapFormat = (date) => date.split('-').reverse().join('-')

export const getDiffInSeconds = (a, b) => Math.abs(new Date(...a) - new Date(...b)) / 1000

export const translateDate = ([y, m, ...rest]) => (new Date(y, m - 1, ...rest))

export const getDate = (
    date,
    options = {weekday: 'long', year: 'numeric', month: 'numeric', day: 'numeric'}
) => translateDate(date).toLocaleDateString('pl-PL', options)

export const getTime = (
    date,
    options = {hour: 'numeric', minute: 'numeric'}
) => translateDate(date).toLocaleTimeString('pl-PL', options)

export const getPeriod = (start, end) => {
    const startDate = getDate(start)
    const endDate = getDate(end)
    const startTime = getTime(start)
    const endTime = getTime(end)

    if (startDate === endDate) {
        return `${startDate} ${startTime}-${endTime}`
    } else {
        return `${startDate} ${startTime} - ${endDate} ${endTime}`        
    }
}