import React from 'react'

const Button = ({ text, callback = null, fluid = false }) => {
    const defaultStyle = {
        backgroundColor: 'dodgerblue',
        color: 'white',
        cursor: 'pointer'
    }
    let style = defaultStyle
    if (fluid) {
        style = {
            ...style,
            width: '100%'
        }
    }
    let props = {
        type: 'submit',
        style,
    }
    if (callback !== null) {
        props = {
            ...props,
            onClick: (e) => { callback(e) }
        }
    }
    return (
        <div style={{ margin: 15 }}>
            <button {...props}>
                {text}
            </button>
        </div>
    )
}

export default Button;