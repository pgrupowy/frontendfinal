import React from 'react'

import { isLogged } from '../services/user'

const possibleRoutes = [
    {
        name: 'Główna',
        path: '/',
        visibilityCheck: () => {
            return !isLogged()
        }
    },
    {
        name: 'Mapa',
        path: '/home',
        visibilityCheck: () => {
            return isLogged()
        }
    },
    {
        name: 'Logowanie',
        path: '/login',
        visibilityCheck: () => {
            return !isLogged()
        }
    },
    {
        name: 'Rejestracja',
        path: '/register',
        visibilityCheck: () => {
            return !isLogged()
        }
    },
    {
        name: 'Wyloguj',
        path: '/',
        visibilityCheck: () => {
            return isLogged()
        },
        onClick: (props) => {
            props.logout()
        }
    },
]

export default class Menu extends React.Component {
    constructor(props) {
        super(props)
    }

    handleClick = (route) => {
        if (route.onClick) {
            route.onClick(this.props)
        }
        this.props.changeRoute(route.path)
    }

    parseRoutes = () => {
        const path = this.props.path
        return possibleRoutes.map((route) => ({
            ...route,
            active: route.path === path,
            visible: route.visibilityCheck()
        }))
    }

    render() {
        const visibleRoutes = this.parseRoutes().filter((route) => route.visible)
        const buttons = visibleRoutes.map((route) => {
            const label = route.active ?
                (
                    <b style={{ cursor: 'default' }} >{route.name}</b>
                ) :
                (
                    <span style={{ cursor: 'pointer' }} onClick={() => { this.handleClick(route) }}>
                        {route.name}
                    </span>
                )
            return <span key={route.name} style={{ padding: '0 10px', display: 'inline-block' }}>{label}</span>
        })

        return (
            <div style={{ alignSelf: 'flex-end', fontSize: 22 }}>
                {this.props.fullName ? <span style={{ display: 'inline-block', color: '#F3F9FE' }}>Witaj {this.props.fullName}!</span> : null}
                {buttons}
            </div>
        )
    }
}