import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { ActionCreators } from '../actions'
import { getDiffInSeconds } from '../utils/date'
import { get } from '../utils/fetch'
import { getUsersForRelatives } from '../services/track'

import { default as Map, drawLines } from './Map';
import TrackList from './TrackList'

const colors = [
    "#0000FF",
    "#00FF00",
    "#FF0000",
    "#0000AA",
    "#00AA00",
    "#AA0000",
    "#00FFFF",
    "#FFFF00",
    "#FF00FF",
    "#00AAAA",
    "#AAAA00",
    "#AA00AA",
]

class TrackMap extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            map: null,
            maps: null,
            colors: colors.map((color, id) => ({
                id: id + 1,
                color,
                free: true
            })),
        }
    }


    componentDidMount = () => {
        this.getTrackList()
    }

    componentWillReceiveProps = (nextProps) => {
        const newRelatives = nextProps.track.newRelatives
        if (newRelatives.length) {
            this.props.trackActions.drawNewRelatives()
            this.drawRelatives(newRelatives)
        }
    }

    handleResponse = (response) => {
        this.props.trackActions.loadTrackList(response)
        this.refreshMap()
    }

    getTrackList = () => {
        get('/rest/tracks', this.handleResponse)
    }

    removers = []

    getFreeColor = () => {
        const freeColor = this.state.colors.find((color) => color.free)
        if (freeColor === undefined) {
            return false
        }
        const updatedColors = this.state.colors.map((color) => {
            if (color.id === freeColor.id) {
                return {
                    ...color,
                    free: false
                }
            } else {
                return color
            }
        })
        this.setState({ colors: updatedColors })
        return freeColor.color
    }

    getFreeColors = (amount = 1) => {
        const colors = [...this.state.colors]
        const freeColors = colors.filter((color) => color.free)
        const occupiedColors = colors.filter((color) => !color.free)
        const selectedColors = freeColors.slice(0, amount)
        const unchangedColors = freeColors.slice(amount, -1)
        this.setState({
            colors: [
                ...occupiedColors,
                ...unchangedColors,
                ...selectedColors.map((color) => ({
                    ...color,
                    free: false
                }))
            ]
        })

        return selectedColors.map((color) => color.color)
    }

    clearMap = () => {
        this.removers.forEach((remover) => remover())
    }

    redrawMap = () => {
        const { tracks, relatives } = this.props.track
        tracks.forEach((track) => {
            if (track.color && !track.hidden) {
                this.addTrack(track)
            }
        })
        relatives.forEach((relative, index) => {
            if (relative.color && !relative.hidden) {
                relative.trackMap.forEach((intersection) => {
                    this.addIntersection(intersection, relative.color)
                })
            }
        })
    }

    resetMap = () => {
        this.props.trackActions.resetColors()
        this.setState({
            colors: colors.map((color, id) => ({
                id: id + 1,
                color,
                free: true
            }))
        })
    }

    refreshMap = () => {
        this.clearMap()
        setTimeout(() => this.redrawMap(), 50)
    }

    drawTrack = (coordinates, color) => {
        const { map, maps } = this.state
        const polyline = drawLines({ map, maps }, coordinates, color)
        this.removers.push(polyline)
    }

    addTrack = (response, additionalOffset = 0) => {
        const randomize = false
        const randomFactor = 0.0002
        let offset = (response.id % 4) * 0.001
        const color = response.color
        let getOffset

        const trackPoints = response.trackPoints
        if (randomize) {
            getOffset = () => offset + Math.random() * randomFactor - randomFactor / 2
        } else {
            getOffset = () => additionalOffset
        }
        const coordinates = trackPoints.map((point) => ({
            lat: point.latitude + getOffset(),
            lng: point.longitude + getOffset()
        }))
        this.drawTrack(coordinates, color ? color : "#000000")
    }

    addIntersection = (intersection, color) => {
        const coordinates = intersection.map((point) => ({
            lat: point.latitude,
            lng: point.longitude
        }))
        this.drawTrack(coordinates, color ? color : "#000000")
    }

    getTrackInfo = (id = 5) => {
        const handleResponse = (response) => {
            const color = this.getFreeColors()[0]
            const data = {
                ...response,
                color
            }
            this.props.trackActions.loadTrack(data)
            this.addTrack(data)
        }
        get('/rest/tracks/' + id, handleResponse)
    }

    onLoad = ({ map, maps }) => {
        this.setState({ map, maps })
    }

    selectTrack = (id) => {
        if (this.props.track.tracks.findIndex(track => track.id === id && track.loaded) === -1) {
            this.getTrackInfo(id)
        } else {
            this.props.trackActions.toggleTrackVisibility(id)
            this.refreshMap()
        }
    }

    drawRelatives = (relatives) => {
        relatives.forEach((relative, index) => {
            getUsersForRelatives(
                relative.id,
                (data) => this.props.trackActions.loadUserForRelative(relative.trackId, relative.id, data)
            )
            relative.trackMap.forEach((intersection) => {
                this.addIntersection(intersection, relative.color)
            })
        })
    }

    splitIntersetions = () => { }

    getRelatives = (trackId = 4) => {
        const handleResponse = (tracks) => {
            const colors = this.getFreeColors(Object.keys(tracks).length)
            const relatives = Object.keys(tracks).map((key, index) => {
                return {
                    trackId,
                    id: parseInt(key, 10),
                    trackPoints: tracks[key],
                    color: colors[index],
                    trackMap: tracks[key].reduce((intersections, currentPoint) => {
                        const lastIntersection = intersections.pop()
                        if (lastIntersection === undefined) {
                            intersections.push([currentPoint])
                        } else {
                            const lastPoint = [...lastIntersection].pop()
                            if (getDiffInSeconds(lastPoint.time, currentPoint.time) < 90) {
                                lastIntersection.push(currentPoint)
                                intersections.push(lastIntersection)
                            } else {
                                intersections.push(lastIntersection)
                                intersections.push([currentPoint])
                            }
                        }
                        return intersections
                    }, [])
                }
            })
            this.props.trackActions.loadRelatives(relatives)
        }
        get('/rest/tracks/' + trackId + '/relatives', handleResponse, (error) => console.error(error))
    }

    hideRelative = (trackId, id) => {
        this.props.trackActions.hideRelative(trackId, id)
        this.refreshMap()
    }

    showRelative = (trackId, id) => {
        this.props.trackActions.showRelative(trackId, id)
        this.refreshMap()
    }

    render() {
        return (
            <div className='flex' style={{
                justifyContent: 'space-evenly',
            }}>
                <div>
                    <TrackList
                        tracks={this.props.track.tracks}
                        relatives={this.props.track.relatives}
                        selectTrack={this.selectTrack}
                        getRelatives={this.getRelatives}
                        hideRelative={this.hideRelative}
                        showRelative={this.showRelative}
                    />
                </div>
                <div className='map'>
                    <Map onLoad={this.onLoad} />
                    {this.props.track.debug ? (
                        <div>
                            <button onClick={this.clearMap} style={{ margin: 10 }}> Clear </button>
                            <button onClick={this.redrawMap} style={{ margin: 10 }}> Redraw </button>
                            <button onClick={this.resetMap} style={{ margin: 10 }}> Reset </button>
                            <button onClick={this.refreshMap} style={{ margin: 10 }}> Refresh </button>
                        </div>
                    ) : null}
                </div>
            </div>
        )
    }
};

function mapStateToProps(state) {
    return {
        track: state.track,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        trackActions: bindActionCreators(ActionCreators.trackActions, dispatch),
    }
}

const ConnectedTrackMap = connect(
    mapStateToProps,
    mapDispatchToProps
)(TrackMap)

export default ConnectedTrackMap