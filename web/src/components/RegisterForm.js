import Form from './Form'
import { swapFormat } from '../utils/date'

export default class RegisterForm extends Form {
    constructor(props) {
        super(props)
        this.state = {
            submitText: 'ZAREJESTRUJ SIĘ',
            fields: [
                {
                    name: 'birthDate',
                    placeholder: 'Data urodzenia',
                    type: 'date'
                },
                {
                    name: 'firstName',
                    placeholder: 'Imię',
                    type: 'text'
                },
                {
                    name: 'lastName',
                    placeholder: 'Nazwisko',
                    type: 'text'
                },
                {
                    name: 'email',
                    placeholder: 'E-mail',
                    type: 'email'
                },
                {
                    name: 'username',
                    placeholder: 'Użytkownik',
                    type: 'text'
                },
                {
                    name: 'password',
                    placeholder: 'Hasło',
                    type: 'password'
                },
                {
                    name: 'passwordRepeated',
                    placeholder: 'Powtórz hasło',
                    type: 'password'
                },
            ],
            values: {
                birthDate: '',
                firstName: '',
                lastName: '',
                email: '',
                username: '',
                password: '',
                passwordRepeated: '',
            }
        }
    }

    prepareData = (data) => ({
        ...data,
        birthDate: swapFormat(data.birthDate)
    })
}
