import React from 'react'
import { getPeriod } from '../utils/date'
import { get } from '../utils/fetch';

export default class TrackList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tracks: [],
            selectTrack: (id) => { this.props.selectTrack(id) },
            getRelatives: (id) => { this.props.getRelatives(id) },
            hideRelative: (trackId, id) => { this.props.hideRelative(trackId, id) },
            showRelative: (trackId, id) => { this.props.showRelative(trackId, id) },
        }
    }

    renderRelative = (data, index) => {
        const toggleVisibility = () => {
            if (data.hidden) {
                this.state.showRelative(data.trackId, data.id)
            } else {
                this.state.hideRelative(data.trackId, data.id)
            }
        }
        return (
            <div key={'r' + index} style={{ ...styles.flex, ...styles.indent }}>
                <span style={{ ...getStyle.number(data.hidden ? null : data.color) }} className='track' onClick={toggleVisibility}>
                    {data.hidden ? 'x' : null}
                </span>
                <span style={{ ...styles.padding }}>{data.user ? data.user.fullName : '?'}</span>
                <span style={{ ...styles.padding }}>{data.trackMap.length + 'X'}</span>
            </div>
        )
    }

    renderRelatives = (trackId) => {
        const relatives = this.props.relatives.filter((realtive) => trackId === realtive.trackId)
        return relatives.map((relative, index) => this.renderRelative(relative, index))
    }

    render() {
        return (
            <div>
                {this.props.tracks.map((track, index) => (
                    <div key={'t' + index} style={{ backgroundColor: '#E4F0FB', marginBottom: 10 }}>
                        <div className='track-container' style={{ ...styles.flex, ...styles.padding }}>
                            <span style={{ ...getStyle.number(track.hidden ? null : track.color) }} className='track' onClick={() => { this.state.selectTrack(track.id) }}>
                                {track.hidden && track.loaded ? 'x' : null}
                            </span>
                            <span style={styles.padding} >
                                <span>{track.name}</span>
                            </span>
                            {track.startDate ? (
                                <span style={styles.padding}>
                                    <span>{getPeriod(track.startDate, track.endDate)}</span>
                                </span>
                            ) : null}
                            {track.relativesLoaded ? null : (
                                <button style={{ ...styles.floatRight, cursor: 'pointer' }} onClick={() => { this.state.getRelatives(track.id) }}>przecięcia</button>
                            )}
                        </div>
                        <div>{this.renderRelatives(track.id)}</div>
                    </div>
                ))}
            </div>
        )
    }
}

const styles = {
    flex: {
        display: 'flex',
        alignItems: 'center'
    },
    padding: {
        padding: 10
    },
    indent: {
        padding: '10px 10px 10px 30px'
    },
    floatRight: {
        marginLeft: 'auto'
    }
}

const getStyle = {
    number: (color) => ({
        marginRight: 10,
        backgroundColor: color,
        cursor: 'pointer',
    })
}