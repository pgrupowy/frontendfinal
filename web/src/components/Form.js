import React from 'react'

import Button from './Button'

export default class LoginForm extends React.Component {
    handleChange = (field, value) => {
        this.setState({
            values: {
                ...this.state.values,
                [field]: value
            }
        })
    }

    prepareData = (data) => data

    handleSubmit = () => {
        const { onSubmit } = this.props
        if (this.checkInput()) {
            const data = this.prepareData(this.state.values)
            onSubmit(data)
        } else {
            alert('Wypełnij puste pola!')
        }
    }

    renderInput = (schema) => {
        const value = this.state.values[schema.name]
        let className = value.length ? 'placeholder mini' : 'placeholder'
        let style = {
            backgroundColor: 'transparent',
            border: 'none',
            boxShadow: 'none',
            borderBottom: '1px solid #0169CE',
            width: '100%'
        }
        if (schema.type === 'date') {
            className = 'placeholder mini'
        }
        return (
            <div style={{ margin: '10px 0', textAlign: 'left' }} key={schema.name}>
                <label htmlFor={schema.name} className={className}>{schema.placeholder}</label>
                <input
                    id={schema.name}
                    style={style}
                    type={schema.type}
                    onChange={(event) => this.handleChange(schema.name, event.target.value)}
                    value={value}
                />
            </div>
        )
    }

    checkInput = () => {
        const fieldNames = this.state.fields.map(field => field.name)
        if (fieldNames.some((fieldName) => this.state.values[fieldName] === '')) {
            return false
        } else {
            return true
        }
    }

    render() {
        return (
            <div style={{ width: 250 }}>
                <form onSubmit={(e) => {
                    e.preventDefault()
                    this.handleSubmit()
                }} >
                    <div>
                        {this.state.fields.map((field) => this.renderInput(field))}
                    </div>
                    <Button
                        fluid={true}
                        text={this.state.submitText}
                    />
                </form>
            </div>
        )
    }
}