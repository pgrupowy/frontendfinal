import Form from './Form'

export default class LoginForm extends Form {
    constructor(props) {
        super(props)
        this.state = {
            submitText: 'ZALOGUJ SIĘ',
            fields: [
                {
                    name: 'username',
                    placeholder: 'Użytkownik',
                    type: 'text'
                },
                {
                    name: 'password',
                    placeholder: 'Hasło',
                    type: 'password'
                },
            ],
            values: {
                username: '',
                password: ''
            }
        }
    }
}