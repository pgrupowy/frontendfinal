import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

export default class Map extends Component {
  static defaultProps = {
    center: { lat: 54.3709836, lng: 18.6127245 },
    zoom: 15
  };

  render() {
    return (
      <GoogleMapReact
        bootstrapUrlKeys={{
          key: 'AIzaSyCEyRJT_BqZtPgvTwpi92yjZ_r8Xkj6o-k',
          language: 'pl'
        }}
        defaultCenter={this.props.center}
        defaultZoom={this.props.zoom}
        onGoogleApiLoaded={this.props.onLoad}
      />
    );
  }
}

const removeLines = (map, polylines) => {
  polylines.forEach((polyline) => {
    polyline.setMap(null)
  })
}

export const drawLines = ({ map, maps }, coordinates = null, color = null) => {
  map.setCenter(new maps.LatLng(coordinates[0].lat, coordinates[0].lng));

  const flightPlanCoordinates = coordinates === null ? [
    { lat: 59.955413, lng: 30.337844 },
    { lat: 60.155413, lng: 30.537844 },
    { lat: 60.255413, lng: 30.337844 },
  ] : coordinates
  const flightPath = new maps.Polyline({
    path: flightPlanCoordinates,
    geodesic: true,
    strokeColor: color === null ? '#FF0000' : color,
    strokeOpacity: 1.0,
    strokeWeight: 3
  });

  flightPath.setMap(map);
  return () => removeLines(map, [flightPath])
};
