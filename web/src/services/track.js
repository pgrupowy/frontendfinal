import { get, post } from '../utils/fetch'

// for later use
import { getCredentials } from './user'

export const getUsersForRelatives = (trackId, onSuccess) => {
    get('/rest/tracks/' + trackId + '/user', onSuccess)
}

export const getTrackList = (handleResponse) => {
    get('/rest/tracks', this.handleResponse)
}

export const uploadTrack = (event) => {
    event.preventDefault()
    const file = event.target.file.files[0]
    const onSuccess = (response) => {
        alert('Przesłano plik')
    }

    const onFail = (error) => {
        alert('Nie udało się przesłać pliku')
        console.error(error)
    }

    const sendFile = () => {
        const fd = new FormData()
        fd.append('file', file)
        post('/rest/tracks', fd, onSuccess, onFail, null, 'multipart/form-data')
    }
    sendFile()
}