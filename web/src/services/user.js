
import { get, post } from '../utils/fetch'

// for later use:
import { bindActionCreators } from 'redux';

import { ActionCreators } from '../actions'
import configureStore from '../reducers'

const actions = (dispatch) => ({
    userActions: bindActionCreators(ActionCreators, dispatch)
})

export const getCredentials = () => {

}

export const isLogged = () => sessionStorage.auth !== undefined

export const login = (dispatch, data, changeRoute) => {
    const onSuccess = (response) => {
        sessionStorage.setItem('auth', JSON.stringify({
            ...response,
            ...data
        }))
        changeRoute('/home')
        dispatch({
            ...response,
            ...data
        })
    }

    const onFail = (error) => {
        alert('Nie udało się zalogować')
        changeRoute('/')
    }

    get('/rest/login', onSuccess, onFail, data)
}

export const logout = (dispatch, changeRoute) => {
    sessionStorage.removeItem('auth')
    dispatch()
    changeRoute('/')
}

export const register = (data, changeRoute) => {
    const onSuccess = (response) => {
        alert('Pomyślnie zarejestrowano')
        changeRoute('/login')
    }

    const onFail = (error) => {
        alert('Nie udało się zarejestrować')
        changeRoute('/')
    }

    post('/registration/user', data, onSuccess, onFail)
}