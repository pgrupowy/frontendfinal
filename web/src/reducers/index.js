import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';

import user from './user'
import track from './track'

const rootReducer = combineReducers({
    user,
    track
})

const configureStore = (initialState) => {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const enhancer = composeEnhancers(
        applyMiddleware(
            thunkMiddleware,
        )
    )
    return createStore(rootReducer, initialState, enhancer)
}

export default configureStore