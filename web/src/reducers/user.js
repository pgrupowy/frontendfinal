import createReducer from '../utils/createReducer'
import * as types from '../actions/types'

const user = (state = { userLogged: false }, action) => {
    switch (action.type) {
        case types.LOGIN:
            const { username, password } = action.data
            return {
                ...state,
                userLogged: true,
                ...action.data
            }

        case types.LOGOUT:
            return {
                userLogged: false
            }

        default:
            return state
    }
}

export default user;