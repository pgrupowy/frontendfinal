import * as types from '../actions/types'

const initialState = {
    tracks: [],
    relatives: [],
    newRelatives: [],
    debug: false
}

const areRelativesEqual = (first, second) => {
    if (first.trackId === second.trackId && first.id === second.id) {
        return true
    } else {
        return false
    }
}

const track = (state = initialState, action) => {
    const loadTrackList = () => {
        const oldTracks = state.tracks
        const newTracks = action.data
        let tracks
        if (oldTracks.length) {
            tracks = newTracks.map((newTrack) => {
                const oldIndex = oldTracks.findIndex((oldTrack) => oldTrack.id === newTrack.id)
                if (oldIndex !== -1) {
                    return {
                        ...newTrack,
                        ...oldTracks[oldIndex]
                    }
                } else {
                    return newTrack
                }
            })
        } else {
            tracks = newTracks
        }

        const relatives = state.relatives.filter((relative) => newTracks.findIndex((track) => track.id === relative.trackId) !== -1)

        return {
            ...state,
            tracks,
            relatives
        }
    }
    const loadTrack = () => {
        const loadedTrack = {
            ...action.data,
            loaded: true,
            hidden: false
        }
        let isNew = true
        const tracks = state.tracks.map((track) => {
            if (track.id !== loadedTrack.id) {
                return track
            } else {
                isNew = false
                return {
                    ...track,
                    ...loadedTrack,
                }
            }
        })
        return isNew ? [...tracks, loadedTrack] : tracks
    }
    const loadRelatives = () => {
        const loadedRelatives = action.data
        const trackId = loadedRelatives.length ? loadedRelatives[0].trackId : null
        const tracks = state.tracks.map((track) => {
            if (track.id !== trackId) {
                return track
            } else {
                return {
                    ...track,
                    relativesLoaded: true
                }
            }
        })
        const newRelatives = state.newRelatives
        const relatives = state.relatives
        loadedRelatives.forEach((loadedRelative) => {
            if (relatives.findIndex(relative => relative.trackId === loadedRelative.trackId && relative.id === loadedRelative.id) === -1) {
                newRelatives.push(loadedRelative)
                relatives.push(loadedRelative)
            }
        })

        return {
            ...state,
            tracks,
            relatives,
            newRelatives
        }
    }
    const resetColors = () => {
        const tracks = state.tracks.map((track) => {
            const { color, ...rest } = track
            return rest
        })
        const relatives = state.relatives.map((track) => {
            const { color, ...rest } = track
            return rest
        })

        return {
            ...state,
            tracks,
            relatives
        }
    }
    const loadUser = () => {
        const { trackId, id, user } = action
        return state.relatives.map((relative) => {
            if (trackId === relative.trackId && id === relative.id) {
                return {
                    ...relative,
                    user
                }
            } else {
                return relative
            }
        })
    }
    const setRelativeVisibility = (visible) => {
        const { trackId, id } = action
        return state.relatives.map((relative) => {
            if (trackId === relative.trackId && id === relative.id) {
                return {
                    ...relative,
                    hidden: !visible
                }
            } else {
                return relative
            }
        })
    }
    const toggleTrackVisibility = () => {
        const { id } = action
        return state.tracks.map((track) => {
            if (id === track.id) {
                return {
                    ...track,
                    hidden: !track.hidden
                }
            } else {
                return track
            }
        })
    }
    switch (action.type) {
        case types.LOAD_TRACK_LIST:
            return loadTrackList()

        case types.LOAD_TRACK:
            return {
                ...state,
                tracks: loadTrack()
            }

        case types.LOAD_RELATIVES:
            return loadRelatives()

        case types.RESET_COLORS:
            return resetColors()

        case types.LOAD_USER_FOR_RELATIVE:
            return {
                ...state,
                relatives: loadUser()
            }

        case types.TOGGLE_TRACK_VISIBILITY:
            return {
                ...state,
                tracks: toggleTrackVisibility()
            }

        case types.HIDE_RELATIVE:
            return {
                ...state,
                relatives: setRelativeVisibility(false)
            }


        case types.SHOW_RELATIVE:
            return {
                ...state,
                relatives: setRelativeVisibility(true)
            }

        case types.DRAW_NEW_RELATIVES:
            return {
                ...state,
                newRelatives: []
            }

        default:
            return state
    }
}

export default track